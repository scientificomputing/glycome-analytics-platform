
# Publishing in the Galaxy Toolshed

https://wiki.galaxyproject.org/ToolShed/PublishTool

- Prepare a correct set of tool files. **Super Important!**
- Go to https://toolshed.g2.bx.psu.edu/.
- Create an account.
- Create a repository.
- Upload your tool files.



## Preparing a correct set of tool files

- Tools are created and tested command line.
- XML wrappers are built
- Also read best practices [here](https://galaxy-iuc-standards.readthedocs.org/en/latest/)
- Test in Galaxy (so far I have mainly done manual testing)
- Publish

## Recommendation for publishing

- Use ./bundle.sh
- Packages will be created in bundle_release/*

```
|-- bundle_release/convert_detect_formats_493998ec52c183b91b8209105f9581c34c30fca5_2016-03-14.tar
|-- bundle_release/extract_display_features_493998ec52c183b91b8209105f9581c34c30fca5_2016-03-14.tar
|-- bundle_release/get_data_493998ec52c183b91b8209105f9581c34c30fca5_2016-03-14.tar
|-- bundle_release/join_subtract_group_493998ec52c183b91b8209105f9581c34c30fca5_2016-03-14.tar
|-- bundle_release/manipulate_493998ec52c183b91b8209105f9581c34c30fca5_2016-03-14.tar
|-- bundle_release/glycan_datatypes_493998ec52c183b91b8209105f9581c34c30fca5_2016-03-14.tar
```

- Upload these packages individually, in the following order
  - glycan_datatypes
  - get_data
  - convert_detect_formats
  - manipulate
  - join_subtract_group
  - extract_display_features


