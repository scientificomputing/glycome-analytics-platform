-------------------------------
Installing to a Galaxy Toolshed
-------------------------------

Use the bundle script, this will create a set of uncompressed tarballs in ./bundle_release/* for upload to the toolshed. ::

    ./bundle.sh

.. note:: bundle does not delete untracked files from the working dir ( you may need them! ). However, some untracked files may interfere with the tool installation. If there is an issue, reclone the project elsewhere and bundle.

.. _Add Tools to the Galaxy Toolshed

Add tools to the Galaxy Toolshed
--------------------------------
- login to your galaxy toolshed instance as user
- select the repositories tab
- in the left tab under the available actions heading. select create new repository name, categorise and describe repo.
- Suggestions

  - glycan_types and upload glycan_datatypes*.tar
  - glycan_convert_formats and upload convert_detect_formats_*.tar
  - glycan_extract_features and upload extract_display_features_*.tar
  - and so forth for the other tarballs in ./bundle_release/*

- click save.
- click upload files to repository
- browse to tar created using bundle.sh. click upload

.. _AddToolFromToolShedTutorial : https://wiki.galaxyproject.org/Admin/Tools/AddToolFromToolShedTutorial

.. _Add our toolshed to your list of Toolsheds:

----------------------------------------------
Add SCRU toolshed to your accessible Toolsheds
----------------------------------------------

Refer to AddToolFromToolShedTutorial_.

Briefly, edit tools_sheds_conf.xml and include SCRU tool shed::

  <?xml version="1.0"?>
  <tool_sheds>
  <tool_shed name="Galaxy main tool shed" url="http://toolshed.g2.bx.psu.edu/"/>
  <tool_shed name="Galaxy test tool shed" url="http://testtoolshed.g2.bx.psu.edu/"/>
  <tool_shed name="SCRU Galaxy test tool shed" url="http://10.10.11.197:9009/"/>
  <tool_shed name="SCRU Galaxy test tool shed" url="http://ec2-52-8-130-141.us-west-1.compute.amazonaws.com:9090"/>
  </tool_sheds>

In config/galaxy.ini (or universe_wsgi.ini for old Galaxy) . Make sure tool_dependency_dir is a valid path, say tool_deps.::

  cd ~galaxy/galaxy-dist; mkdir tool_deps

config/galaxy.ini (universe_wsgi.ini)::

  ...
  # If this option is not set to a valid path, installing tools with dependencies
  # from the Tool Shed will fail.
  tool_dependency_dir = tool_deps
  ...

Restart the galaxy daemon::

     sh run.sh --reload

.. _ Add tools to Galaxy

-------------------
Add tools to Galaxy
-------------------

- login to your galaxy instance as user with admin permissions
- select the admin tab
- in the left tab under the Toolsheds heading. select search and browse toolsheds.
- select SCRU galaxy tool shed or other appropriate toolshed
- search for tools, e.g. glycan
- select tool, click on preview and install.
- choose install to galaxy (top right)
- choose to install to a new toolpanel, for example

  - Install glycan_convert_formats to Glycan Convert Formats
  - Install glycan_extract_features to Glycan Extract Features
  - and so forth for the other tools


- then click install (bottom of page)


=============
Configuration
=============

----------------------------------
To update tools in Galaxy Toolshed
----------------------------------

- login to your galaxy toolshed instance as user
- select the repositories tab
- in the left tab under the all repositories actions heading. select browse by category OR under the repositories I can change heading choose repositories I own
- select the repository
- on the top right choose repository action and choose upload files to repository
- browse for updated tarball.
- choose upload
- now update the tools in your Galaxy instance

-------------------------
To update tools in Galaxy
-------------------------

- select the admin tab
- in the left tab under the Server heading. select manage installed toolshed repos
- on the top right select udpate toolshed status and wait
- tools with changes will get a red exclamation icon.
- on the dropdown for the tool, choose get updates.

-------------------------------------
To reinstall broken tool dependencies
-------------------------------------

- in your page for your tool, scroll down to missing tool dependencies
- select the dependency and uninstall it.
- then select dependency and reinstall it.
- if this doesn't work, look at error log and read the logs in "galaxy-dist/$tool_dependency_dir/$dependency/$user/$version/$repotip/\*.log"

---------------------------
To remove tools from Galaxy
---------------------------

- login to your galaxy instance as user with admin permissions
- select the admin tab
- in the left tab under the server heading. selectA manage installed toolshed repositories
- select the dropdown arrow on the tool of interest.
- select deactive or uninstall
- to uninstall check the checkbox and then select the deactivate or uninstall button.


------------------------------------
To remove tools from Galaxy Toolshed
------------------------------------

- login to your galaxy toolshed instance as user with admin permissions
- select the admin tab
- in the left tab under the repositories heading. select browse all repositories.
- select the dropdown arrow on the tool of interest.
- select delete.

