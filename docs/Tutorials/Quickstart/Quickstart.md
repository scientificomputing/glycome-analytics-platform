
# Quickstart and Example Tutorial 

Browse to the [testing site](http://ec2-52-26-82-234.us-west-2.compute.amazonaws.com:8080) (this should be up but we can't guarantee it. notify via the issue tracker please).

## Find and analyse glycans related to ST6Gal1

The objective is to search the KEGG glycan database, find glycan related to the enzyme ST6Gal1 and then analyse these.

### The interface

The Galaxy GAP interface consists of four panels. The top panel gives access to workflows, login (not available in the test instance). The left panel is the tool menu and shows available tools. The right panel is the history of logs all data and calculations undertaken by the user. The middle panel or main panel is for running tools and examining datasets. 

![Alt text](img/GAP_main.png "The Galaxy GAP main interface")

### Optional : Create a new history 

If you are logged in, create a new history. Otherwise skip this step.

![Click on the history panel](img/history.png "Click on the history panel")

![Create a new history](img/history_new.png "Create a new history")

Rename the history.

![Rename the history](img/history_rename.png "Rename the history")

![Renamed history](img/history_renamed.png "Renamed history")


### GAP cross reference

Navigate to the tool menu. In the get data panel, choose the GAP crossref tool. 

![GAP Get Data panel](img/GAP_getdata_panel.png "GAP Get Data panel")

![GAP KEGG cross reference](img/GAP_crossref_panel.png "GAP KEGG cross reference")

Choose the Target Database as the KEGG glycan database. Choose the entry to be ec:2.4.99.1 and execute.

![GAP KEGG cross reference tool details](img/GAP_crossref_tool.png "GAP KEGG cross reference tool details")

The job will be submitted.

![cross reference job submitted](img/job1sub.png "cross reference job submitted")

The result will show up in the history panel. A text file containing two columns is returned. The first column contains the KEGG enzyme number, the second column contains the KEGG glycan number.

![cross reference job result](img/job1result.png "cross reference job result")


### GAP get glycans

Download the glycans in KCF from KEGG using the GAP Get KEGG glycans tool.

![GAP Get KEGG glycans tool panel](img/GAP_getgly_panel.png "GAP Get KEGG glycans tool panel")

Choose the correct history item, in this case "1: linked entries" and execute. 

![GAP Get KEGG glycans tool details](img/GAP_getgly_tool.png "GAP Get KEGG glycans tool details")

The result will show in the history panel. Click on the eye icon to view the data in the main panel. 

![get glycan job result](img/job2result.png "get glycan job result")

Click on the history item name to expand the item and show more details within the history panel. 

![get glycan job result expanded in panel](img/job2result_expand.png "get glycan job result expanded in panel")

### Get Images of the Glycans

In the Convert panel, choose the GAP KCF to Image tool.

![GAP Convert Data panel](img/GAP_convert_panel.png "GAP Convert Data panel")

![GAP KCF to image tool panel](img/GAP_kcftoimage_panel.png "GAP KCF to image tool panel")

Choose the correct history item, in this case "2: KEGG kcf" and execute. 

![GAP KCF to image tool details](img/GAP_kcftoimage_tool.png "GAP KCF to image tool details")

The result will show in the history panel. Click on the eye icon to view the data in the main panel. This data is a list of glycan id and corresponding 2D tree representation.

![get image job result](img/job3result.png "get image job result")

![Images of glycans related to ST6GAl1](img/job3result_main.png "Images of glycans related to ST6GAl1")


### Multiple Carbohydrate Alignment with Weights (MCAW) analysis

In the Glycome Features panel, choose the MCAW tool.

![GAP Glycome Features panel](img/GAP_feature_panel.png "GAP Glycome Features panel")

![GAP MCAW tool panel](img/GAP_MCAW_panel.png "GAP MCAW tool panel")

Choose the correct history item, in this case "2: KEGG kcf" and execute. 

![GAP MCAW tool details](img/GAP_MCAW_tool.png "GAP MCAW tool details")


The result may take a while before it becomes available in the history panel. Click on the eye icon to view the data in the main panel.  

![MCAW job waiting](img/job4result_waiting.png "MCAW job waiting")

![MCAW job result](img/job4result.png "MCAW job result")

The result of the alignment is a profile KCF (not shown here) and image which summarises the features of the aligned glycans. 

If not indicated (or a white square with black outline) the anomeric information is not known. A '0' indicates a wildcard and denotes that the linkage information is not known. 'E'/'End' denotes the end of a sequence, that is a certain percentage of aligned glycans terminate. Numbers are in percent, for each node these sum to unity.  '-'/'Gap' denotes a gap, that is a certain percentage of glycans do not contain the specified node contents, rather the parent of this node is directly connected to its grandchild.

In this example 22.2% of the glycans analysed are core fucosylated.

![MCAW image for glycans related to ST6Gal1](img/job4result_main.png "MCAW image for glycans related to ST6Gal1")

