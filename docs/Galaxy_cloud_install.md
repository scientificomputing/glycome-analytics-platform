# Installing Galaxy in Amazon EC2 Cloud

## Other Resources discussing Cloud Installation

http://training.bioinformatics.ucdavis.edu/docs/2012/05/AWS/AWS-galaxy.html
https://wiki.galaxyproject.org/Cloud
https://wiki.galaxyproject.org/CloudMan

## Installation

### 1. Set up EC2 instance on AWS
Installing directly to AWS manually, as using CloudMan may incur charges to my free usage.

### 2. Sign up for AWS and Login
Explained on Amazon Web Services website.

### 3. Create an EC2 instance
Choose Amazon Linux and select free tier (this will offer a micro instance only).
Click next as required.

####  Add storage
Set the default storage to 8Gb and deselect delete on termination.
Add additional storage of 5Gb and deselect delete on termination.

#### Tag the instance
Name : Galaxy with Glycan Tools

#### Firewall
Allow SSH via port 22
Add a custom TCP rule for port 8080 (HTTP for Galaxy)
If needed add a custom TCP rule for port 9090 (HTTP for Galaxy Toolshed).
Allow from 0.0.0.0 (everywhere) and ignore AWS warnings.

**Note that as the server is open to the world, you must switch off develop and debug settings and not store any super secretive data here.**

#### Private keys and deployment
- Create and use personal private key for SSH access. (info on AWS site, use puttygen to convert key and then associate this with yout putty session)
- Deploy the instance.
- Set up billing alerts for when charges >0.


### 4. Install Galaxy on instance
SSH to instance (details on IP etc. via AWS dashboard). See more on [AWS docs](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/AccessingInstances.html?icmpid=docs_ec2_console)

#### Update and installation prerequisites
```
sudo yum update
sudo yum install make automake gcc gcc-c++ gcc-gfortran cmake mercurial libcairo2-devel python-devel
sudo yum install git tmux
```
#### Use a virtualenv
```
cd ~
virtualenv --no-site-packages galaxy_env
source ./galaxy_env/bin/activate
```

#### Download and install Galaxy
Install to /home/ec2-user/ (the new way)::

```
git clone https://github.com/galaxyproject/galaxy/
cd galaxy
git checkout -b master origin/master
```

Install to /home/ec2-user (the old way)::

```
hg clone https://bitbucket.org/galaxy/galaxy-dist/
cd ~/galaxy-dist
hg pull
hg update
```

### 5. First time startup
```
tmux new -s galaxy 
sh run.sh (will create initial config files and install python packages)
```
Wait a bit until serving on 127.0.0.1:8080. Stop the server (ctrl-C).

If it doesn't exist yet, create config/galaxy.ini::

` cp config/galaxy.ini.sample config/galaxy.ini`

Edit config/galaxy.ini, change host 127.0.0.1 to 0.0.0.0
Restart Galaxy::

`sh run.sh --reload`


### 5. Register as user and admin

Login to your server:8080 and register as a user.

Edit config/galaxy.ini to make yourself admin and disallow registration

```
admin_users = chrisbarnettster@gmail.com

# Force everyone to log in (disable anonymous access).
#require_login = False

# Allow unregistered users to create new accounts (otherwise, they will have to
# be created by an admin).
allow_user_creation = False
```

Note still allowing anonymous access (unless server is abused).

### 6. Configuration
More info at https://wiki.galaxyproject.org/Admin/Config/Performance/ProductionServer

Disable the developer settings for now by editing config/galaxy.ini

```
debug = False # Disable middleware that loads the entire response in memory for displaying debugging information in the page. If left enabled, the proxy server may timeout waiting for a response or your Galaxy process may run out of memory if it's serving large files.
use_interactive = False # Disables displaying and live debugging of tracebacks via the web. Leaving it enabled will expose your configuration (database password, id_secret, etc.).
```

restart Galaxy.

Other options for perfomance improvements include using postgresql, including additional compute nodes.

