[TOC]
# Troubleshooting

## Software requirements

### pyqt4 requirements
pyqt4 requirements for ete2 used in dendrogram tools are not installable into a virtualenv via pip

## General

### installing tool in galaxy - this repository  is not installed correctly

although the tool repo may seem valid (the toolshed repo may look ok), there is usually an error with tool upload. "check $YOURGALAXYINSTANCE/tool_shed_webapp.log"

The other thing that can happen is that you are copying tools from a cluttered development directory. Try recloning the project and branch elsewhere, rebundle and upload.

### upload to toolshed, some tools seem missing
check "$YOURGALAXYINSTANCE/tool_shed_webapp.log" . Usually there has been an error with the tools installations, for instance a file parsing SEVERE ERROR.

The other thing that can happen is that you are copying tools from a cluttered development directory. Try recloning the project and branch elsewhere, rebundle and upload.

### virtualenv in galaxy takes a while to setup
Creating the virtual env can sometimes take a few minutes.

### data selection issues in Galaxy
Sometimes after tool updates (esp. delete and reinstalls) some tools data selection will not work properly. For example all tool selections point to incorrect input data options.

Fix, restart galaxy::

  sh run.sh --reload

### tools not showing up in Galaxy
It is important to check that XML wrappers are wellformed. Broken XML means that the tool will not show up in galaxy.
Check XML validity using an XML linter ::

  ./validate_xml.sh


