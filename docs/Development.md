# Development Notes

## Config, Syntax and best practices for Galaxy Tools
- https://wiki.galaxyproject.org/Admin/Tools/ToolConfigSyntax
- https://wiki.galaxyproject.org/Tools/BestPractices
- http://wiki.sb-roscoff.fr/ifb/index.php/Tool_Integration_Short_Tutorial

## Things to remember to do
- Remember to increment tool versions with each change of the wrapper
- Remember to write and run unit tests
- Remember to validate XML

## validate rst in tools
Found [this site](http://rst.ninjs.org/) to be helpful.

## best practices
https://galaxy-iuc-standards.readthedocs.org/en/latest/best_practices/tool_xml.html#tool-versions

Tool IDs should contain only [a-z0-9_-].
Multiple words should be separated by underscore or dashes
Suite tools should prefix their ids with the suite name. E.g. bedtools_*



## Examples
- see the Chemical Toolbox example - https://github.com/bgruening/galaxytools/tree/master/chemicaltoolbox


