# file to be sourced using tmux as follows by the galaxy user: 
# tmux -f name_of_this_file 

new-session -d -s galaxy 
attach -t galaxy 
 
split-window -d -v 
split-window -d -v 
split-window -d -v 
 
 
select-layout tile 
 
select-pane -t 0 
send-keys 'printf "\033]2;%s\033\\" GALAXY' C-m 
send-keys "echo START GALAXY" C-m 
send-keys "source ~/galaxy_env/bin/activate" C-m 
send-keys "cd ~/galaxy" C-m 
send-keys "sh run.sh --reload --log-file=galaxy.log" C-m 
 
select-pane -t 1 
send-keys 'printf "\033]2;%s\033\\" GALAXYTOOLSHED' C-m 
send-keys "echo START GALAXYTOOLSHED" C-m 
send-keys "source ~/galaxy_env/bin/activate" C-m 
send-keys "cd ~/galaxy" C-m 
send-keys "sh run_tool_shed.sh --reload" C-m 
 
select-pane -t 2 
send-keys 'printf "\033]2;%s\033\\" GALAXYLOG' C-m 
send-keys "echo TAIL GALAXY LOG" C-m 
send-keys "cd ~/galaxy" C-m 
send-keys "tail -f galaxy.log -s 2" C-m 
 
select-pane -t 3 
send-keys 'printf "\033]2;%s\033\\" GALAXYTOOLSHEDLOG' C-m 
send-keys "echo TAIL GALAXYTOOLSHED LOG" C-m 
send-keys "cd ~/galaxy" C-m 
send-keys "tail -f tool_shed_webapp.log -s 2" C-m 

