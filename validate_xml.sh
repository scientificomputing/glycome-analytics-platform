#__license__ = "MIT"

git clone https://github.com/JeanFred/Galaxy-XSD.git
for i in `find . -name "*.xml" | grep -v ".idea"`
do

 xmllint --noout --schema Galaxy-XSD/galaxy.xsd $i

done
