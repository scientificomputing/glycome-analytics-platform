#__license__ = "MIT"

# 0. Set Params and Clean up 
BUNDLE_DIR="../bundle_release"
BUNDLE_DEV_DIR="../bundle_dev"
EXCLUSIONS='--exclude "virtualpy2.7" --exclude "*.pyc" --exclude "*.pyo" --exclude "bundle" --exclude ".idea" --exclude ".hg" --exclude ".git"'


TOOLBASEDIR="tools"
SUITE="GAP"
SUITED=${SUITE}dev

## go to toolsdir
pushd tools

## clean up old tars

rm -rf $BUNDLE_DIR
rm -rf $BUNDLE_DEV_DIR

## remake bundle dir

mkdir $BUNDLE_DIR
mkdir $BUNDLE_DEV_DIR



# 1. FOR RELEASE. Create types, and galaxy groups. 

## create tar for datatypes
tar -cf $BUNDLE_DIR/${SUITE}_datatypes_`git log -1 --format="%H" | cut -c 1-8`_`date +%F`.tar ../datatypes $EXCLUSIONS

## loop through all the tools and bundle together with dependency

for tools in convert_detect_formats extract_display_features get_data join_subtract_group manipulate 
do

# # tar -cf $BUNDLE_DIR/${SUITE}_${tools}_`git log -1 --format="%H" | cut -c 1-8`_`date +%F`.tar -T /dev/null $EXCLUSIONS
tar -cf $BUNDLE_DIR/${SUITE}_${tools}_`git log -1 --format="%H" | cut -c 1-8`_`date +%F`.tar tool_dependencies.xml $EXCLUSIONS

for dir in $tools/*/
do
    dir=${dir%*/}
    echo ${tools}/${dir##*/}
    tar -rf $BUNDLE_DIR/${SUITE}_${tools}_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar ${tools}/${dir##*/} $EXCLUSIONS
done
done

# 2. FOR DEV TESTING

## create tar for python dependencies
tar -cf $BUNDLE_DEV_DIR/${SUITED}_tool_deps_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar tool_dependencies.xml $EXCLUSIONS


## create tar for datatypes and dependencies
tar -cf $BUNDLE_DEV_DIR/${SUITED}_glycan_deps_and_types_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar ../datatypes tool_dependencies.xml $EXCLUSIONS

### loop through all the tools and bundle each one individually 
for tools in convert_detect_formats extract_display_features get_data join_subtract_group manipulate 
do
for dir in $tools/*/
do
    dir=${dir%*/}
    echo ${tools}/${dir##*/}
    tar -cf $BUNDLE_DEV_DIR/${SUITED}_${tools}_${dir##*/}_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar tool_dependencies.xml $EXCLUSIONS
    tar -rf $BUNDLE_DEV_DIR/${SUITED}_${tools}_${dir##*/}_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar ${tools}/${dir##*/} $EXCLUSIONS
done
done


### loop through all the tools and bundle together
tar -cf $BUNDLE_DEV_DIR/${SUITED}_all_tools_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar README.rst $EXCLUSIONS

for tools in convert_detect_formats extract_display_features get_data join_subtract_group manipulate 
do
for dir in $tools/*/
do
    dir=${dir%*/}
    echo ${tools}/${dir##*/}
    tar -rf $BUNDLE_DEV_DIR/${SUITED}_all_tools_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar ${tools}/${dir##*/} $EXCLUSIONS
done
done

### loop through all the tools and bundle together with dependency
tar -cf $BUNDLE_DEV_DIR/${SUITED}_all_tools_with_dep_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar tool_dependencies.xml $EXCLUSIONS

for tools in convert_detect_formats extract_display_features get_data join_subtract_group manipulate 
do
for dir in $tools/*/
do
    dir=${dir%*/}
    echo ${tools}/${dir##*/}
    tar -rf $BUNDLE_DEV_DIR/${SUITED}_all_tools_with_dep_`git log -1 --format="%H"| cut -c 1-8`_`date +%F`.tar ${tools}/${dir##*/} $EXCLUSIONS
done
done

# 3. SUMMARY 

## Development 
MESSAGE="Created the following DEVELOPMENT tarballs for upload to Galaxy Toolshed in `pwd`:"
echo " "
echo -e "\E[07;05;31m$MESSAGE\e[m"
tree -fCt --noreport $BUNDLE_DEV_DIR

## Release
MESSAGE="Created the following RELEASE tarballs for upload to Galaxy Toolshed in `pwd`:" 
echo " "
echo -e "\E[07;05;32m$MESSAGE\e[m"
#echo "Created the following RELEASE tarballs for upload to Galaxy Toolshed in `pwd`:"
tree -fCt --noreport $BUNDLE_DIR

popd

