[TOC]

# 1. munge_msa.py

Read msa and strip out glycan data in LinearCode format.


## Works with Galaxy?
Yes. see [munge_msa.xml](munge_msa.xml)

## Command line usage
```
../../virtualpy/bin/activate
python munge_msa.py $input $output
```


## Unit Testing?
Yes.

```
../../virtualpy/bin/activate
python  test_msa_to_lc.py
```
