__license__ = "MIT"

import unittest
import munge_msa as munge


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_empty_stream(self):
        m = munge.munge_linearcode_from_msa(None)
        self.assertEqual(m, [])
        m = munge.munge_linearcode_from_msa([])
        self.assertEqual(m, [])


    def test_msa_version2(self):
        import StringIO

        v2input = ["# .msa version 002",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   "1579.78 1579.85 0  100 0.0  0.0  0.0  2 5 0 0 0 .10 10 C (Ma3(Ma3(Ma6)Ma6)Mb4GNb4GN) (0) (carbNlink_35518_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        m = munge.munge_linearcode_from_msa(v2handle)
        self.assertEqual(m, ['(Ma3(Ma3(Ma6)Ma6)Mb4GNb4GN)'])


    def test_msa_version3(self):
        import StringIO

        v3input = ["# .msa version 003", "# O-glycans",
                   "# pred  observ off chg rank intens dif %dif sd nac hex fuc na ng S dom good",
                   " 534.29  534.39 0 Na+    6 2984.0 0.0  0.0  0.0   1  1  0  0  0  0.0 0.0 0 ((A??)AN) (0) ()",
                   " 779.41  779.54 0 Na+  142  463.8 0.0  0.0  0.0   2  1  0  0  0  0.0 0.0 0 ((A??)AN,GN) (0) ()"]
        v3handle = StringIO.StringIO('\n'.join(v3input))
        m = munge.munge_linearcode_from_msa(v3handle)
        self.assertEqual(m, ['((A??)AN)', '((A??)AN,GN)'])

    def test_msa_versionunknown(self):
        import StringIO

        vinput = ["# .msa version xyz"]
        vhandle = StringIO.StringIO('\n'.join(vinput))
        with self.assertRaisesRegexp(ValueError, "version"):
            m = munge.munge_linearcode_from_msa(vhandle)

    def test_space_in_linear(self):
        import StringIO

        v2input = ["# .msa version 002", "# O-glycans",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   " 895.46  895.37 0  100 0.0  0.0  0.0  1 1 0 1 0 .10 10 C (A??AN NN) (0) (carbOlink_35827_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        m = munge.munge_linearcode_from_msa(v2handle)
        self.assertEqual(m, ['(A??AN,NN)'])

    def test_first_missing_bracket(self):
        import StringIO

        v2input = ["# .msa version 002", "# O-glycans",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   " 895.46  895.37 0  100 0.0  0.0  0.0  1 1 0 1 0 .10 10 C A??AN NN) (0) (carbOlink_35827_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        m = munge.munge_linearcode_from_msa(v2handle)
        self.assertEqual(m, ['(A??AN,NN)'])

    def test_final_missing_bracket(self):
        import StringIO

        v2input = ["# .msa version 002", "# O-glycans",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   " 895.46  895.37 0  100 0.0  0.0  0.0  1 1 0 1 0 .10 10 C (A??AN NN (0) (carbOlink_35827_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        m = munge.munge_linearcode_from_msa(v2handle)
        self.assertEqual(m, ['(A??AN,NN)'])

    def test_final_missing_bracket_and_incorrect_extension_region(self):
        import StringIO

        v2input = ["# .msa version 002", "# O-glycans",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   " 895.46  895.37 0  100 0.0  0.0  0.0  1 1 0 1 0 .10 10 C (A??AN (0) (carbOlink_35827_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        m = munge.munge_linearcode_from_msa(v2handle)
        self.assertEqual(m, ['(A??AN)'])

    def test_missing_bracket1(self):
        import StringIO

        v2input = ["# .msa version 002", "# O-glycans",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   " 895.46  895.37 0  100 0.0  0.0  0.0  1 1 0 1 0 .10 10 C (NN??A??(Fa3)GN??(NN??A??(Fa3GN??)Ma3(NN??A??GN??(NN??A??GN??)Ma6)Mb4GNb4(Fa6) GN) (0) (carbOlink_35827_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        with self.assertRaises(ValueError):
            m = munge.munge_linearcode_from_msa(v2handle)

    def test_missing_bracket2(self):
        import StringIO

        v2input = ["# .msa version 002", "# O-glycans",
                   "# pred m   observ m  off rank   dif     %dif  sd nac hex fuc neuac neugc  S good",
                   " 895.46  895.37 0  100 0.0  0.0  0.0  1 1 0 1 0 .10 10 C (NN??A??(Fa3)GN??(NN??A??(((Fa3)GN??)Ma3(NN??A??GN??(NN??A??GN??)Ma6)Mb4GNb4(Fa6)GN) (0) (carbOlink_35827_A)"]
        v2handle = StringIO.StringIO('\n'.join(v2input))
        with self.assertRaises(ValueError):
            m = munge.munge_linearcode_from_msa(v2handle)

def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()


# handle = StringIO.StringIO(''.join(item))
