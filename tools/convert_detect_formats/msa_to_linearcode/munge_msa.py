__author__ = "Chris Barnett"
__version__ = "0.4"
__license__ = "MIT"


def check_count(linearcode,left="(",right=")",diff=False):
    countleft=linearcode.count(left)
    countright=linearcode.count(right)
    if diff:
        return countleft-countright
    if countleft==countright:
        return True
    else:
        return False

def munge_linearcode_from_msa(inputstream):
    """
    will read the input msa file and strip out linearcode. Also attempts to clean damaged linearcode
    :param inputstream (a file object):
    :return: list_of_linearcode
    """
    if inputstream is None or inputstream == []:
        return []
    linearcode_list = []
    for line in inputstream:
        if line[0] == "#":
            if "version" in line:
                if "002" in line:
                    version = "002"
                elif "003" in line:
                    version = "003"
                else:
                    raise ValueError("unknown version of msa file", line)
            pass
        else:
            split = line.split()
            splitindices = {"002": (15, 16), "003": (17, 18)}
            linearcode = split[splitindices[version][0]]

            # first check if missed out some of the linearcode
            # Example where this is useful #(A??AN ; A??AN,NN)
            import re
            missing = split[splitindices[version][1]].lstrip()
            if re.search('[a-zA-Z]', missing) is not None:
                linearcode=','.join((linearcode,missing))

            if check_count(linearcode): # equal number of brackets,append linearcode
                linearcode_list.append(linearcode)
            elif check_count(linearcode,diff=True) == 1 or check_count(linearcode,diff=True) == -1: # one bracket too many
                if linearcode[0]=="(" and linearcode[-1]!=")": # check is there is a left bracket in position 1 and no right bracket in final position
                    linearcode_list.append(linearcode+")")
                elif linearcode[0]!="(" and linearcode[-1]==")": # check is there is no left bracket in position 1 and a right bracket in final position
                    linearcode_list.append("("+linearcode)
                else:
                    raise ValueError("missing brackets in linearcode entry", linearcode,check_count(linearcode,diff=True))

            else: # "("+x = ")" x>1 or x < -1
                raise ValueError("missing brackets in linearcode entry", linearcode,check_count(linearcode,diff=True))

    return linearcode_list


if __name__ == "__main__":
    import sys

    try:
        inputname = sys.argv[1]
        outputname = sys.argv[2]
    except Exception as e:
        raise Exception(e, "Please pass an input and output filename as arguments")
    instream = file(inputname, 'r')
    outstream = file(outputname, "w")
    try:
        for item in munge_linearcode_from_msa(instream):
            outstream.write(item + "\r\n")
    except Exception as e:
        raise
    finally:
        instream.close()
        outstream.close()
