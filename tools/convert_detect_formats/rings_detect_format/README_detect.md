
[TOC]

# 1. glycan_sniff.py


**Guesses the glycan file format using the tools from RINGS**

The tool can be used to guess the format and return the result as text. (returns format)
Given a format, the tool can be used to confirm whether a sequence is in the given format. (returns True/False)


## Works with Galaxy?
Yes. see [glycan_sniff.xml](glycan_sniff.xml)


## Command line usage
```
../../virtualpy/bin/activate
python glycan_sniff.py -i $input $format
```

## Help
```
../../virtualpy/bin/activate
python glycan_sniff.py -h
```

## Unit Testing?
Yes. Use test_rings_detect_format.py

```
../../virtualpy/bin/activate
python test_rings_detect_format.py
```

