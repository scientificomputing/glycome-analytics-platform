__license__ = "MIT"

import unittest
import glycan_sniff as gs


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        # dictionary of example glycan formats
        self.types = {"KCF": """
ENTRY      12345     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
""", "LINEARCODE": """((A??)AN)""", "LinearCode": """GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN""",
                      "LINUCS": """[][D-GLC]{[(6+1)][B-D-GLCP]{[(3+1)][B-D-GLCP]{}[(6+1)][B-D-GLCP]{[(6+1)][B-D-GLCP]{[(3+1)][B-D-GLCP]{}[(6+1)][B-D-GLCP]{}}}}}""",
                      "IUPAC": """GlcNAc(b1-2)Man(a1-6)[Gal(b1-4)GlcNAc(b1-2)""", "GLYDE2": """
<?xml version="1.0" encoding="UTF-8" ?>
<GlydeII>
<molecule subtype="glycan" id="From_GlycoCT_Translation">
<residue subtype="base_type" partid="1" ref="http://www.monosaccharideDB.org/GLYDE-II.jsp?G=b-dglc-HEX-1:5" />
<residue subtype="substituent" partid="2" ref="http://www.monosaccharideDB.org/GLYDE-II.jsp?G=n-acetyl" />
<residue subtype="base_type" partid="3" ref="http://www.monosaccharideDB.org/GLYDE-II.jsp?G=a-lgal-HEX-1:5|6:d" />
<residue subtype="base_type" partid="4" ref="http://www.monosaccharideDB.org/GLYDE-II.jsp?G=b-dgal-HEX-1:5" />
<residue subtype="base_type" partid="5" ref="http://www.monosaccharideDB.org/GLYDE-II.jsp?G=a-dgro-dgal-NON-2:6|1:a|2:keto|3:d" />
<residue subtype="substituent" partid="6" ref="http://www.monosaccharideDB.org/GLYDE-II.jsp?G=n-acetyl" />
<residue_link from="2" to="1">
<atom_link from="N1H" to="C2" to_replace="O2" bond_order="1" />
</residue_link>
<residue_link from="3" to="1">
<atom_link from="C1" to="O3" from_replace="O1" bond_order="1" />
</residue_link>
<residue_link from="4" to="1">
<atom_link from="C1" to="O4" from_replace="O1" bond_order="1" />
</residue_link>
<residue_link from="5" to="4">
<atom_link from="C2" to="O3" from_replace="O2" bond_order="1" />
</residue_link>
<residue_link from="6" to="5">
<atom_link from="N1H" to="C5" to_replace="O5" bond_order="1" />
</residue_link>
</molecule>
</GlydeII>
""", "glycoct": """
<?xml version="1.0" encoding="UTF-8"?>
<sugar version="1.0">
  <residues>
    <basetype id="1" anomer="x" superclass="hex" ringStart="-1" ringEnd="-1" name="x-dman-HEX-x:x">
      <stemtype id="1" type="dman" />
    </basetype>
    <basetype id="2" anomer="a" superclass="hex" ringStart="1" ringEnd="5" name="a-dman-HEX-1:5">
      <stemtype id="1" type="dman" />
    </basetype>
    <basetype id="3" anomer="b" superclass="hex" ringStart="1" ringEnd="5" name="b-dglc-HEX-1:5">
      <stemtype id="1" type="dglc" />
    </basetype>
    <substituent id="4" name="n-acetyl" />
    <basetype id="5" anomer="b" superclass="hex" ringStart="1" ringEnd="5" name="b-dgal-HEX-1:5">
      <stemtype id="1" type="dgal" />
   </basetype>
    <basetype id="6" anomer="a" superclass="hex" ringStart="1" ringEnd="5" name="a-dman-HEX-1:5">
      <stemtype id="1" type="dman" />
    </basetype>
    <basetype id="7" anomer="b" superclass="hex" ringStart="1" ringEnd="5" name="b-dglc-HEX-1:5">
      <stemtype id="1" type="dglc" />
    </basetype>
    <substituent id="8" name="n-acetyl" />
    <basetype id="9" anomer="b" superclass="hex" ringStart="1" ringEnd="5" name="b-dgal-HEX-1:5">
      <stemtype id="1" type="dgal" />
    </basetype>
  </residues>
  <linkages>
    <connection id="1" parent="1" child="2">
      <linkage id="1" parentType="o" childType="d">
        <parent pos="3" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="2" parent="2" child="3">
      <linkage id="2" parentType="o" childType="d">
        <parent pos="2" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="3" parent="3" child="4">
      <linkage id="3" parentType="d" childType="n">
        <parent pos="2" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="4" parent="3" child="5">

      <linkage id="4" parentType="o" childType="d">
        <parent pos="4" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="5" parent="1" child="6">
      <linkage id="5" parentType="o" childType="d">
        <parent pos="6" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="6" parent="6" child="7">
      <linkage id="6" parentType="o" childType="d">
        <parent pos="2" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="7" parent="7" child="8">
      <linkage id="7" parentType="d" childType="n">
        <parent pos="2" />
        <child pos="1" />
      </linkage>
    </connection>
    <connection id="8" parent="7" child="9">
      <linkage id="8" parentType="o" childType="d">
        <parent pos="4" />
        <child pos="1" />
      </linkage>
    </connection>
  </linkages>
</sugar>
"""}

        # dictionary of example unknown / not applicable formats
        self.unknown_types = {"somejson": """{"numlinkages": 8, "userglycome": "user_glycome21_11_2013-breastcancer", "format": "glycoct_condensed", "notes": "serum from stage I breast cancer patient", "numnodes": 9, "commonname": "", "source": "clinchem", "date": "09:56 21/11/2013", "data": ["RES\n", "1b:x-dglc-HEX-1:5\n", "2b:x-dglc-HEX-1:5\n", "3b:x-dman-HEX-1:5\n", "4b:x-dman-HEX-1:5\n", "5b:x-dman-HEX-1:5\n", "6b:x-dman-HEX-1:5\n", "7b:x-dman-HEX-1:5\n", "8s:n-acetyl\n", "9s:n-acetyl\n", "LIN\n", "1:1o(-1+1)2d\n", "2:2o(-1+1)3d\n", "3:3o(-1+1)4d\n", "4:3o(-1+1)5d\n", "5:5o(-1+1)6d\n", "6:5o(-1+1)7d\n", "7:2d(2+1)8n\n", "8:1d(2+1)9n\n", "\n"]}
""", "sometext": """
        ab1234 fkk
""", "somedigraph": """
digraph HumanGly {
A
B
C
D
E
F
G
H
I
J
K
A -> B
A -> C
C -> D
D -> E
D -> F
E -> G
E -> H
G -> I
F -> J
J -> K
}

        """}

        pass

    def tearDown(self):
        pass

    def test_empty_stream(self):
        m = gs.sniff_glycan(None)
        self.assertEqual(m, "")
        m = gs.sniff_glycan([])
        self.assertEqual(m, "")
        m = gs.sniff_glycan("")
        self.assertEqual(m, "")

    def test_type_unknown(self):
        import StringIO
        #
        for item in self.unknown_types.items():
            kchandle = StringIO.StringIO(''.join(item))
            self.assertEqual(gs.sniff_glycan(kchandle), "")


    def test_type_kcf(self):
        import StringIO
        #
        kcinput = """
ENTRY      12345     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""
        kchandle = StringIO.StringIO(''.join(kcinput))
        m = gs.sniff_glycan(kchandle)
        self.assertEqual(m, "KCF")

    def test_type_known_bug_kcf(self):
        import StringIO
        #
        kcinput = """
ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""
        kchandle = StringIO.StringIO(''.join(kcinput))
        m = gs.sniff_glycan(kchandle)
        self.assertEqual(m, "LinearCode")  # this is a bug in RINGS!


    def test_is_a_kcf(self):
        import StringIO

        kcinput = """
ENTRY      12345     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""
        kchandle = StringIO.StringIO(''.join(kcinput))
        self.assertEqual(gs.sniff_glycan(kchandle, is_a="KCF"), True)


    def test_type(self):
        import StringIO

        for keys in self.types.keys():
            kchandle = StringIO.StringIO(''.join(self.types[keys]))
            self.assertEqual(gs.sniff_glycan(kchandle).upper(), keys.upper())

    def test_is_a_(self):
        import StringIO

        for keys in self.types.keys():
            kchandle = StringIO.StringIO(''.join(self.types[keys]))
            self.assertEqual(gs.sniff_glycan(kchandle, is_a=keys), True)

    def test_type_known_bug_glycoct_condensed_is_not_recognised(self):
        import StringIO
        #
        kcinput = """
RES
1b:b-dglc-HEX-1:5
2s:n-acetyl
3b:b-dglc-HEX-1:5
4s:n-acetyl
5b:b-dman-HEX-1:5
6b:a-dman-HEX-1:5
7b:b-dglc-HEX-1:5
8s:n-acetyl
9b:a-dman-HEX-1:5
10b:b-dglc-HEX-1:5
11s:n-acetyl
LIN
1:1d(2+1)2n
2:1o(4+1)3d
3:3d(2+1)4n
4:3o(4+1)5d
5:5o(3+1)6d
6:6o(2+1)7d
7:7d(2+1)8n
8:5o(6+1)9d
9:9o(2+1)10d
10:10d(2+1)11n

"""
        kchandle = StringIO.StringIO(''.join(kcinput))
        m = gs.sniff_glycan(kchandle)
        self.assertEqual(m, "")  # this is a bug in RINGS!


def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()


