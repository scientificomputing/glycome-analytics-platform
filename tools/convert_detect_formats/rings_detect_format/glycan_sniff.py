__author__ = "Chris Barnett"
__version__ = "0.3"
__license__ = "MIT"


def sniff_glycan(inputstream, is_a=None):
    """
    Passes input glycan format to RINGS and returns format.
    If is_a is specified then return boolean
    :rtype : text, or Boolean if is_a is specified or None if broken (maybe should be False)
    :param inputstream: inputstream with glycan struct. (not sure for multiple...  in any of LinearCode, KCF etc. (can be mixed))
    :param is_a: If not None then test for the file type specified.
    """
    if inputstream is None or inputstream == [] or inputstream == "":
        return ""
    try:
        from suds.client import Client

        url = 'http://rings.t.soka.ac.jp/axis2/services/Utilities?wsdl'
        client = Client(url)
        response = client.service.DeterminingForm(inputstream.read())
        if response is None:
            return ""
        fmt = response.array[0]  # response is an object,just access the attributes
        if is_a is not None and "" != is_a:
            if is_a.upper() == fmt.upper():  # compare fmt names and avoid case issues
                return True
            else:
                return False
        else:
            return fmt
    except Exception as e:
        raise IOError(e)


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", action="store", type="string", dest="i", default="input",
                      help="input any glycan file (input)")
    parser.add_option("-f", action="store", type="string", dest="f", default=None,
                      help="return true if is the specified format KCF)")
    (options, args) = parser.parse_args()
    instream = file(options.i, 'r')
    print sniff_glycan(instream, options.f)
