
[TOC]

# 1. post_kcf_to_image.py

**This tool reads a kcf file (containing glycans) and returns images in CFG notation using the server at RINGS. Images are presented in html and the first kcf glycan is returned as a png.**

## Works with Galaxy?
Yes. see [post_kcf_to_image.xml](post_kcf_to_image.xml)


## Command line usage
```
../../virtualpy/bin/activate
python post_kcf_to_image.py $input $pngoutput $htmloutput
```

## Help
```
../../virtualpy/bin/activate
python post_kcf_to_image.py -h
```

## Unit Testing?
Yes. Use test_rings_kcf_to_image.py

```
../../virtualpy/bin/activate
python test_rings_kcf_to_image.py
```
