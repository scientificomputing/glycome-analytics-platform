__license__ = "MIT"

import unittest
import post_kcf_to_image as ki


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_empty_stream(self):
        m = ki.post_rings_kcf_to_image(None)
        self.assertEqual(m, [])
        m = ki.post_rings_kcf_to_image([])
        self.assertEqual(m, [])

    def test_not_a_kcf(self):
        import StringIO

        kcinput = """
GNb2(Ab4GNb4)Ma3(Ab4GNb2(Fa3(Ab4)GNb6)Ma6)Mb4GNb4GN
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = ki.post_rings_kcf_to_image(kchandle)
        self.assertIn("<html>", h)
        self.assertIn("<img", h)
        self.assertIn("KCF to image Results", h)
        with self.assertRaises(IOError):
            ki.get_first_image_from_html(h)

    def test_broken_kcf(self):
        import StringIO

        kcinput = """
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = ki.post_rings_kcf_to_image(kchandle)
        self.assertIn("<html>", h)
        self.assertIn("<img", h)
        self.assertIn("KCF to image Results", h)
        with self.assertRaises(IOError):
            ki.get_first_image_from_html(h)


    def test_one_kcf(self):
        import StringIO

        kcinput = """
ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = ki.post_rings_kcf_to_image(kchandle)
        self.assertIn("<html>", h)
        self.assertIn("<img", h)
        self.assertIn("KCF to image Results", h)
        i = ki.get_first_image_from_html(h)
        self.assertIsNotNone(i)

    def test_multi_kcf(self):
        import StringIO

        kcinput = """
ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
ENTRY       G00005                      Glycan
NODE        6
            1   PP-Dol     15     1
            2   GlcNAc      8     1
            3   GlcNAc      0     1
            4   Man        -9     1
            5   Man       -16     7
            6   Man       -16    -6
EDGE        5
            1     2:a1    1
            2     3:b1    2:4
            3     4:b1    3:4
            4     5:a1    4:6
            5     6:a1    4:3
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = ki.post_rings_kcf_to_image(kchandle)
        self.assertIn("<html>", h)
        self.assertIn("<img", h)
        self.assertIn("KCF to image Results", h)
        i = ki.get_first_image_from_html(h)
        self.assertIsNotNone(i)

        # now assert that the correct number of  multiple images returned
        from BeautifulSoup import BeautifulSoup
        soup = BeautifulSoup(h)
        tags = soup.findAll(name='img')  # have to use explicit name= , as source html is damaged *by me..
        imgsrc = (list(tag['src'] for tag in tags))
        # have sent over 3 kcf files, expect 3 images and the rings logo image
        self.assertEqual(len(imgsrc),int(4))

def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()


