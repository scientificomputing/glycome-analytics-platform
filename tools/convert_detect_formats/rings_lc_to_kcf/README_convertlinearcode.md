
[TOC]

# 1. post_linear_to_kcf.py

**This tool reads a linearcode file and returns KCF output.**

## Works with Galaxy?
Yes. see [post_linear_to_kcf.xml](post_linear_to_kcf.xml)


## Command line usage
```
../../virtualpy/bin/activate
python post_linear_to_kcf.py $input $output
```

## Unit Testing?
Yes. Use test_rings_lc_to_kcf.py

```
../../virtualpy/bin/activate
python test_rings_lc_to_kcf.py
```
