__author__ = "Chris Barnett"
__version__ = "0.3"
__license__ = "MIT"


def post_rings_linear_to_kcf(inputstream, outputformat="text"):
    """
    posts linearcode to the linearcode converter at RINGS
    http://rings.t.soka.ac.jp/cgi-bin/tools/utilities/LinearCodetoKCF/linearcode_to_kcf_index.pl
    'http://rings.t.soka.ac.jp/cgi-bin/tools/utilities/LinearCodetoKCF/LinearCode_to_KCF.pl'
    :param inputstream: file object which is read and then passed to the textarea
    :param outputformat: html or text
    """
    import urllib

    if inputstream is None or inputstream == []:
        return []

    # URL to post to
    # changed url to action url found in the form source of the linearcodetokcf page
    url = 'http://rings.t.soka.ac.jp/cgi-bin/tools/utilities/LinearCodetoKCF/LinearCode_to_KCF.pl'
    lcode = inputstream.read()
    file = ""
    values = dict(datasetname='default', LinearCode=lcode, LinearCodefile=file, type=outputformat, submit='SUBMIT')
    try:
        html = urllib.urlopen(url, urllib.urlencode(values)).readlines()
    except Exception as e:
        raise e
    return ''.join(html[13:])  # slightly hacky way to ignore the first part of the file


if __name__ == "__main__":
    import sys

    try:
        inputname = sys.argv[1]
        outputname = sys.argv[2]
    except Exception as e:
        raise Exception(e, "Please pass an input and output filename as arguments")
    instream = file(inputname, 'r')
    outstream = file(outputname, "w")
    try:
        outstream.write(post_rings_linear_to_kcf(instream))
    except Exception as e:
        raise
    finally:
        instream.close()
        outstream.close()
