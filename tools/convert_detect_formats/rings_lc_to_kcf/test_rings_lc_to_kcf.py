__license__ = "MIT"

import unittest
import post_linear_to_kcf as lc


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_empty_stream(self):
        m = lc.post_rings_linear_to_kcf(None)
        self.assertEqual(m, [])
        m = lc.post_rings_linear_to_kcf([])
        self.assertEqual(m, [])

    def test_not_linearcode(self):
        import StringIO

        nonlcinput = ["# .msa version 002", "2323"]
        nonlchandle = StringIO.StringIO('\n'.join(nonlcinput))
        m = lc.post_rings_linear_to_kcf(nonlchandle)
        self.assertIn("NODE        0", m)

    def test_linearcode_basic(self):
        import StringIO

        lcinput = ["(Ma3(Ma3(Ma6)Ma6)Mb4GNb4GN)"]
        lchandle = StringIO.StringIO('\n'.join(lcinput))
        m = lc.post_rings_linear_to_kcf(lchandle)
        self.assertIn("NODE        7", m)
        self.assertIn("glcnac", m)
        self.assertIn("man", m)

    def test_linearcode_unknownlinkage(self):
        import StringIO

        lcinput = ["((A??)AN)"]
        lchandle = StringIO.StringIO('\n'.join(lcinput))
        m = lc.post_rings_linear_to_kcf(lchandle)
        self.assertIn("NODE        2", m)
        self.assertIn("galnac", m)
        self.assertIn("gal", m)
        self.assertIn("EDGE        1", m)

    def test_linearcode_unknownposition(self):
        import StringIO

        lcinput = ["(A??AN,NN)"]
        lchandle = StringIO.StringIO('\n'.join(lcinput))
        m = lc.post_rings_linear_to_kcf(lchandle)
        self.assertIn("NODE        2", m)  # as unknown are ignored by this code
        self.assertIn("galnac", m)
        self.assertIn("gal", m)
        self.assertIn("EDGE        1", m)

    def test_linearcode_multiple(self):
        import StringIO

        lcinput = ["(A??AN,NN)", "(Ma3(Ma3(Ma6)Ma6)Mb4GNb4GN)"]
        lchandle = StringIO.StringIO('\n'.join(lcinput))
        m = lc.post_rings_linear_to_kcf(lchandle)
        self.assertIn("NODE        7", m)  # as unknown are ignored by this code
        self.assertIn("NODE        2", m)  # as unknown are ignored by this code
        self.assertIn("///", m)


def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()


