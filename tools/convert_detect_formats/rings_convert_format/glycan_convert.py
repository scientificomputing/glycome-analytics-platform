__author__ = "Chris Barnett"
__version__ = "0.3"
__license__ = "MIT"


def convert_glycan(inputstream, format, text=True):
    """
    passes inputstream to RINGS and returns requested glycan format
    allowed formats are KCF, LinearCode... etc. but not all conversion are supported.
    Unsupported conversion return None

    :rtype :  ((object,text,None) and  format
    :param inputstream: inputstream with glycan struct. (not sure for multiple...  in any of LinearCode, KCF etc. (can be mixed))
    :param format: desired output format
    :param text: True yields raw text, False yields suds.umx.types object from RINGS
    """
    if inputstream is None or inputstream == [] or inputstream == "":
        raise IOError("empty input stream")
    if format is None or format == "":
        return inputstream, None  # <-- should not return format of inputstream. YAGNI. user should use detect_formats tool
    try:
        from suds.client import Client

        url = 'http://rings.t.soka.ac.jp/axis2/services/Utilities?wsdl'
        client = Client(url)
        if text:
            response = client.service.ConvertGlycan_Str(inputstream.read(), format, 'glycan')
        else:
            response = client.service.ConvertGlycan(inputstream.read(), format, 'glycan')
        if response is None or str(response.strip()) == '':
            raise IOError("empty response")
            #return None, None # response is None then None is returned.
        return response, format  # Again not checking format
    except Exception as e:
        raise IOError(e)


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", action="store", type="string", dest="i", default="input",
                      help="input any glycan file (input)")
    parser.add_option("-f", action="store", type="string", dest="f", default="KCF",
                      help="format to convert to (KCF)")
    parser.add_option("-n", action="store_true", dest="n",default=False,
                      help="turn off textual output format (not recommended)")
    parser.add_option("-o", action="store", type="string", dest="o", default="output",
                      help="output glycan file (output)")
    (options, args) = parser.parse_args()
    try:
        instream = file(options.i, 'r')
    except Exception as e:
        raise IOError(e, "the input  file specified does not exist. Use -h flag for help")
    with open(options.o,'w') as f:
        f.write( str(convert_glycan(instream, options.f, not options.n)[0]))
