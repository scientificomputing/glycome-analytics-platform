
[TOC]

# 1. post_glycan_convert.py

**RECOMMENDED**

**Use the NEW glycan convert tools from RINGS**

## Works with Galaxy?
Yes. see [post_glycan_convert.xml](post_glycan_convert.xml)

## Formats supported
Can only convert to KCF, GlycoCT {condensed}, LinearCode, Wurcs, IUPAC and MDL MOl. (last checked 5/10/2015, last checked 25/02/2015)
JSON output is requested by default. This returns a JSON output and the glycan in its new format.

| File Format (Down) , Converts to (Across) | WURCS | KCF  | LinearCode | IUPAC | GlydeII | GlycoCT XML | GlycoCT (condensed) | MDLMol | LINUCS |
|:-----------------------------------------:|-------|------|------------|-------|---------|-------------|---------------------|--------|--------|
|                   WURCS                   | -     | -    | -          | -     | -       | -           | -                   | -      | -      |
|                    KCF                    | X     | -    | X,U        | X     | -       | -           | X                   | X,U    | U      |
|                 LinearCode                | X     | X,U  | -          | X     | -       | -           | X                   | X      | -      |
|                   IUPAC                   | X     | X,U  | X          | -     | -       | -           | X                   | X      | -      |
|                  Glyde II                 | -     | X, U | X          | X     | -       | -           | X                   | X      | -      |
|                GlycoCT XML                | -     | X, U | X          | X     | -       | -           | X                   | X      | -      |
|            GlycoCT (condensed)            | X     | X, U | X          | X     | -       | -           | -                   | X      | -      |
|                   MDLMol                  | -     | -    | -          | -     | -       | -           | -                   | -      | -      |
|                   LINUCS                  | -     | U    | -          | -     | -       | -           | -                   | -      | -      |


- does not convert
C - converts using NEW Ring convert tool
U - converts using specific utility on RINGS mainpage


## Command line usage
```
../../virtualpy/bin/activate
python post_glycan_convert.py -i $input -f $changeformat -t $outputformat -o $output -j $jsonoutput
```

## Help
```
../../virtualpy/bin/activate
python post_glycan_convert.py -h
```

## Unit Testing?
Yes. Use test_rings_convert_via_post.py

```
../../virtualpy/bin/activate
python test_rings_convert_via_post.py
```

# 2. glycan_convert.py
**Convert between multiple glycan formats using the RINGS SOAP service**

Rather use the other tool from RINGS.


## Works with Galaxy?
Yes. see [glycan_convert.xml](glycan_convert.xml)

## Command line usage

```
../../virtualpy/bin/activate
python glycan_convert.py -i $input -f $changeformat -o $output
```

## Help
```
../../virtualpy/bin/activate
python glycan_convert.py -h
```

## Unit Testing?
Yes. Use test_rings_convert_format.py

```
../../virtualpy/bin/activate
python test_rings_convert_format.py
```

