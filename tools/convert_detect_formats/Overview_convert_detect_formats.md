
# Overview of data conversion tools

These are tools to convert between glycan data formats.

## Resource for Informatics of Glycomes at Soka (RINGS)

- convert format tools
Convert between glycan formats using the convert tools at RINGS.
[Convert tools](rings_convert_format/README_convert.md)

- detect format tools
Detect glycan formats using the detect tool at RINGS.
[Detect tools](rings_detect_format/README_detect.md)

- convert to kcf to image
Convert kcf to image using the tool at RINGS.
[Convert KCF](rings_kcf_to_image/README_convertKCF.md)

- linearcode to kcf
Convert from linearcode format to kcf format using the tool at RINGS.
[Convert linearcode](rings_lc_to_kcf/README_convertlinearcode.md)


## Other resources

- convert from msa to linearcode
Convert from mass spec annotated (msa) format to linearcode format.
[Convert msa](msa_to_linearcode/README_convertmsa.md)

