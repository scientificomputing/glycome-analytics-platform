[TOC]

# html_combiner.py

This code provides a vertical split view of two html files.


## Works with Galaxy?
Surprisingly yes. See [html_combiner.xml](html_combiner.xml)

## Command line usage

```
../../virtualpy/bin/activate
python html_combiner.py $input1 $input2 $output
```

## Unit Testing?
No.

