__license__ = "MIT"
__version__ = "0.1"

# really rudimentary way to compare two html files
# Going to make a new html containing a 2 column, 2 row table. Left cell will contain contents of file1, right cell contents of file 2
# Will strip <html>, </html> and tags by hand for now.

if __name__ == "__main__":
    import sys

    try:
        inone = file(sys.argv[1], "read").readlines()
        for line in inone:
            if "<html" in line or "</html" in line:
                inone.remove(line)
            elif "<head" in line or "</head" in line:
                inone.remove(line)
            elif "Content" in line:
                inone.remove(line)
            elif "<link" in line or "<title" in line or "<meta":
                inone.remove(line)
            elif "</link" in line or "</title" in line or "</meta":
                inone.remove(line)
        # print inone
        intwo = file(sys.argv[2], "read").readlines()
        for line in intwo:
            if "<html" in line or "</html" in line:
                intwo.remove(line)
            elif "<head" in line or "</head" in line:
                intwo.remove(line)
            elif "Content" in line:
                intwo.remove(line)
            elif "<link" in line or "<title" in line or "<meta":
                intwo.remove(line)
            elif "</link" in line or "</title" in line or "</meta":
                intwo.remove(line)
        out = file(sys.argv[3], "w")
        header = """
<html>
<head>
<title>Compare html files </title>
</head>
<table style="width:700px border="1" padding:15px;>
<thead>
<tr>
  <th><h1> File 1 </h1></th> <th> <h1> File 2 </h1> <th>
</tr>
</thead>
<tr>
  <th>"""
        out.write(header)
        out.write("".join(inone))
        inbetween = """
 </th> <th> """
        out.write(inbetween)
        out.write("".join(intwo))
        footer = """
</th>
</tr>
</table>  """
        out.write(footer)

    except Exception as e:
        raise e
    finally:
        out.close()

