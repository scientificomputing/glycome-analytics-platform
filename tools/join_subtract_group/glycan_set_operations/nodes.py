__license__ = "MIT"

class node(object):
    """ A tree-ish class. Each node instance contains children multiple children.
    """
    # init with no children
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.children = []
        self.set_mapping_id(-1)
        # self.mapping_id = int(-1)

    # def __init__(self, id, children = []):
    # self.id = id
    # self.children = children

    def add_child(self, newchild):
        self.children.append(newchild)

    def __repr__(self, level=0):
        ret = "\t" * level + repr(str(self.id) + " " + str(self.name)) + "\n"
        for child in self.children:
            ret += child.__repr__(level + 1)
        return ret

    def iter(self, level=0):
        #  ret = "\t" * level + repr(self.id + " " + self.name) + "\n"
        ret = [self.id + self.name]
        for child in self.children:
            ret.append(child.name)
        for child in self.children:
            ret.append(child.iter(level + 1))
        return ret

    # Depth-first traversal
    # by default preorder traversal (i.e. root, child, subchild)
    # but post order traversal return as going deep first
    # and return from deepest node first so prints subchild, child, root
    def __iter__(self):
        return next(self)

    def next(self):
        #  yield self
        for i in self.children:
            for ii in i:
                yield ii
        yield self

    def bf_traverse(self):
        thislevel = [self]
        while thislevel:
            nextlevel = list()
            for n in thislevel:
                print n.name, n.id
                for children in n.children:
                    nextlevel.append(children)
                print
            thislevel = nextlevel

    def breadth_first(self):
        """Traverse the nodes of a tree in breadth-first order.
        The first argument should be the tree root; children
        should be a function taking as argument a tree node and
        returning an iterator of the node's children.
         loosely based on http://code.activestate.com/recipes/231503-breadth-first-traversal-of-tree/
        # note comments on the page that say this is not BFS, but it works for me
        # checked vs my other bf_traverse() which prints
        """
        yield self
        last = self
        for node in self.breadth_first():
            for child in node.children:
                yield child
                last = child
            if last == node:
                return

    def return_id(self):
        # assume id's are unique
        return self.id

    def return_name(self):
        return self.name

    # to allow my parent (who I don't know of) to set my mapping id
    def set_mapping_id(self, id):
        self.mapping_id = int(id)

    def return_mapping_id(self):
        return self.mapping_id

    def return_child(self):
        return self.children

    def return_numchildren(self):
        return len(self.children)

    def len(self):
        size = 0
        for node in self.breadth_first():
            size += 1
        return size

    def is_equal(self, tree):
        # confirm both are node objects
        if type(self) != type(tree):
            return False
        # at least should be the same size
        if tree.len() != self.len():
            return False
        # iterate in parallel and test node name
        # also test number of children (should also test names but too lazy)
        for x, y in zip(self.breadth_first(), tree.breadth_first()):
            if x.name != y.name:
                return False
            if x.return_numchildren() != y.return_numchildren():
                return False

        return True

    def hash(self):
        # simplistic hashing function
        hashnum = 0
        for node in self.breadth_first():
            # convert each char in node.name to ascii integer equivalent
            for index in range(len(node.name)):
                try:
                    hashnum += ord(node.name[index])
                except:  # sometimes the character is considered length, ignoring this for now two https://docs.python.org/2/library/functions.html#ord
                    pass
        return hashnum


if __name__ == '__main__':

    root = node("1", "root")
    child1 = node("2", "child1")
    child2 = node("3", "child2")
    schild1 = node("4", "schild1")
    root.add_child(child1)
    root.add_child(child2)
    child1.add_child(schild1)

    print root
    print child1
    print "Use Iterator"
    for n in root:
        print "Default Order Depth First -- ", n
    print "BF traverse print example"
    root.bf_traverse()
    print "BF traverse yield example"
    for n in root.breadth_first():
        print "BF traversal via yield-- ", n.return_name()
    print "Length"
    print root.len()
    print "Equality"
    print root.is_equal(root)
    root2 = node("1", "root")
    child21 = node("2", "child1")
    child22 = node("3", "child2")
    schild21 = node("4", "schild1")
    root2.add_child(child21)
    root2.add_child(child22)
    child21.add_child(schild21)
    print root.is_equal(root2)
    child21.add_child(schild21)
    print root.is_equal(root2)
    print root.hash()


