__license__ = "MIT"
__version = "0.3.10"


def read_meta_kcf(inputstream):
    """
    :param inputstream: the kcf file
    :return: list of kcfs
    """

    if inputstream is None or inputstream == [] or inputstream == "":
        raise IOError("empty input stream")

    list_of_kcf_paragraphs = []
    kcfpara = None
    for line in inputstream:
        if "ENTRY" in line:
            kcfpara = [line]
        elif "///" in line:
            kcfpara.append(line)
            list_of_kcf_paragraphs.append(kcfpara)
        else:
            if kcfpara is not None:
                kcfpara.append(line)
    # . sometimes kcf has no /// or final kcf in many has no ////, so add it
    if kcfpara not in list_of_kcf_paragraphs:
        list_of_kcf_paragraphs.append(kcfpara)

    return list_of_kcf_paragraphs  # why this list. easier to deal with each glycan as an individual item in the list


def read_meta_kcf_list_to_set(list_of_kcfs):
    from glycan import Glycan

    import StringIO

    list_of_glycans = []
    for item in list_of_kcfs:
        glycan = Glycan()
        handle = StringIO.StringIO(''.join(item))
        glycan.readsequence(handle, True)
        list_of_glycans.append(glycan)
    return set(list_of_glycans)


def glycan_set_to_kcf_meta_string(glycan_set):
    kcflist = []
    for gly in glycan_set:
        kcflist.append(gly.sequence["sourcedata"])
    return "".join(kcflist)


def glycan_set_comparison(fileseta, filesetb):
    """
    :param fileseta:
    :param filesetb:
    :return: union, intersection, relative diffab and relative diff ba
    """
    import logging as logging

    logging.basicConfig(filename="logfile", level=logging.DEBUG)
    # logging.debug('These are the %s additional glycans', additional_glycans)
    seta = read_meta_kcf_list_to_set(read_meta_kcf(fileseta))
    setb = read_meta_kcf_list_to_set(read_meta_kcf(filesetb))
    # for gly in setA:
    # print gly, gly.sequence["sourcedata"]
    unionab = seta.union(setb)
    intersectionab = seta.intersection(setb)
    rab = seta - setb
    rba = setb - seta
    return glycan_set_to_kcf_meta_string(seta), glycan_set_to_kcf_meta_string(setb), glycan_set_to_kcf_meta_string(
        unionab), glycan_set_to_kcf_meta_string(intersectionab), glycan_set_to_kcf_meta_string(
        rab), glycan_set_to_kcf_meta_string(rba)


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("--setA", action="store", type="string", dest="setA", default="setA.kcf",
                      help="input glycan set in kcf format (this is set A)")
    parser.add_option("--setB", action="store", type="string", dest="setB", default="setB.kcf",
                      help="input glycan set in kcf format (this is set B)")
    parser.add_option("--uniqueA", action="store", type="string", dest="uniqueA", default="uniquesetA.kcf",
                      help="unique set A (kcf output) ")
    parser.add_option("--uniqueB", action="store", type="string", dest="uniqueB", default="uniquesetB.kcf",
                      help="unique set B (kcf output) ")
    parser.add_option("--union", action="store", type="string", dest="union", default="ABunion.kcf",
                      help="union of sets (kcf output) ")
    parser.add_option("--intersect", action="store", type="string", dest="intersect", default="ABintersection.kcf",
                      help="intersection of sets (kcf output) ")
    parser.add_option("--differenceAB", action="store", type="string", dest="differenceAB", default="AminusB.kcf",
                      help="relative difference of sets A - B (kcf output) ")
    parser.add_option("--differenceBA", action="store", type="string", dest="differenceBA", default="BminusA.kcf",
                      help="relative difference of sets B - A (kcf output) ")
    (options, args) = parser.parse_args()

    try:
        inputA = options.setA
        inputB = options.setB
    except Exception as e:
        raise Exception(e, "Please pass  2 kcf data files")
    astream = file(inputA, 'r')
    bstream = file(inputB, 'r')
    try:
        uniquesetA, uniquesetB, union, intersection, AB, BA = glycan_set_comparison(astream, bstream)
        with open(options.uniqueA, "w") as f:
            f.write(uniquesetA)
        with open(options.uniqueB, "w") as f:
            f.write(uniquesetB)
        with open(options.union, "w") as f:
            f.write(union)
        with open(options.intersect, "w") as f:
            f.write(intersection)
        with open(options.differenceAB, "w") as f:
            f.write(AB)
        with open(options.differenceBA, "w") as f:
            f.write(BA)
    except Exception as e:
        raise e
