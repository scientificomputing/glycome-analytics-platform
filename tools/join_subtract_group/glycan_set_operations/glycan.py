__author__ = 'cbarnett'
__license__ = "MIT"


class Glycan(object):
    """rudimentary glycan class - alpha"""

    def __init__(self):
        self._sequence = None

    def readsequence(self, filename, is_handle=False):
        """ try read this file and populate _sequence{}"""
        if not is_handle:
            try:
                with open(filename):
                    pass
            except IOError:
                print 'Oh dear.', filename, ' doesn\'t exist'
                return None
        self._sequence = {}
        # # record file name
        self.sequence["sourcename"] = filename
        # # slurp up sequence data from file
        if not is_handle:
            self.sequence["sourcedata"] = open(str(filename)).read()
        else:
            filename.seek(0)
            self.sequence["sourcedata"] = filename.read()
        # # identify type
        self.sequence["sourcetype"] = self.file_typer(filename, is_handle)
        # interesting way to have a case statement for functions
        #  options = {'kcf': self.kcf, 'glycoct': self.glycoct, 'unknown': self.unknown}
        #  self.sequence["internaltype"] = 'kcf'
        options = {'kcf': self.kcf, 'glycoct': self.glycoct, 'glydeii': self.glydeii, 'unknown': self.unknown}
        self.sequence["internaltype"] = options[self.sequence["sourcetype"]](filename, is_handle)
        options = {'kcf': self.kcfdata, 'glycoct': self.glycoct, 'glydeii': self.glydeii, 'unknown': self.unknown}
        self.sequence["internaltypedata"] = options[self.sequence["sourcetype"]](filename, is_handle)
        return self.sequence

    @staticmethod
    def file_typer(filename, is_handle=False):
        """
type the glycan formats (inspired by Galaxy)

        """
        firstline = ""
        secondline = ""
        try:
            if not is_handle:
                f = open(filename, "r")
            else:
                f = filename
                f.seek(0)
            firstline = f.readline().upper()  # note we are uppercasing here to avoid CasE SenSitIVity
            secondline = f.readline().upper()  # note we are uppercasing here to avoid CasE SenSitIVity
        except IOError:
            print 'Oh dear.', filename, ' doesn\'t exist'
        if "ENTRY" in firstline and "GLYCAN" in firstline:
            return 'kcf'
        elif "xml".upper() in firstline:
            if "glydeii".upper() in secondline:
                return 'glydeii'
            elif "sugar".upper() in secondline:
                return 'glycoct'
            else:
                return 'unknown'
        else:
            return 'unknown'

    @staticmethod
    def pretty_print_stats(statname, stats):
        print(statname + '\n')
        for key in stats:
            print(' |_ Node %s \n' % key)
            for neighbour in stats[key]:
                results = [int(i) for i in stats[key][neighbour]]
                print('  |__ %s %s \n' % (neighbour, sum(results)))

    @staticmethod
    def kcf(filename, is_handle=False, returntree=False):
        """
         original code written by S. Silubonde.
         improved KCF support by CB
        :param filename:
        :param is_handle:
        :return:
        """
        import nodes as nds

        edgedict = {}
        nodedict = {}
        aclist = []
        nodelist = []
        alpbeta = []
        vertices_ = []
        edges_ = []
        if not is_handle:
            f = open(filename, "r")
        else:
            f = filename
            f.seek(0)
        try:
            for line in f:
                if "ENTRY" in line:
                    minimal_kcf = [line]
                elif "NODE" in line:
                    _, totalnodes = line.split()
                    totalnodes = int(totalnodes)
                    minimal_kcf.append(line)
                    for inodes in range(0, totalnodes):
                        nodeline = f.next()
                        minimal_kcf.append(nodeline)
                        nodeline = nodeline.split()
                        nodelist.append(nodeline[1])
                        nodedict[nodeline[0]] = nodeline[1]
                        vertices_.append(nds.node(nodeline[0], nodeline[1]))
                elif "EDGE" in line:
                    _, totaledges = line.split()
                    minimal_kcf.append(line)
                    totaledges = int(totaledges)
                    for inodes in range(0, totaledges):
                        edgeline = f.next()
                        minimal_kcf.append(edgeline)
                        edgeline = edgeline.split()
                        donor = edgeline[1]
                        acceptor = edgeline[2]
                        edgedict[donor] = acceptor
                        if ":" in donor:
                            donornde, conform = donor.split(":")
                        else:
                            donornde = donor
                            conform = "?"
                        aclist.append(acceptor)
                        alpbeta.append(conform)
                        if ":" in acceptor:
                            accnde, acccarb = acceptor.split(":")
                        else:
                            accnde = acceptor
                            acccarb = "?"
                        # create edge array
                        edges_.append(nds.node(edgeline[0], (donornde, conform, accnde, acccarb)))
                elif "///" in line:
                    break  # only one kcf allowed
                    # minimal_kcf.append(line)
        except StopIteration:
            pass
        for e in edges_:
            # print e.name[0]
            e.add_child(vertices_[int(e.name[0]) - 1])
            parent = int(e.name[2]) - 1
            vertices_[parent].add_child(e)
        # print entire connected tree / graph
        # print vertices_[0].generate_mappings()
        #  print Glycan.pretty_print_stats("kcf graph",vertices_[0])
        try:
            if not returntree:
                return Glycan.jsonify('kcf', totalnodes, totaledges, nodelist, edgedict)
            else:
                return vertices_[0]

        except Exception as e:
            raise e

    @staticmethod
    def kcfdata(filename, is_handle=False):
        return Glycan.kcf(filename=filename, is_handle=is_handle, returntree=True)

    @staticmethod
    def glycoct(filename, is_handle=False):
        return filename

    @staticmethod
    def glydeii(filename, is_handle=False):
        return filename

    @staticmethod
    def unknown(filename, is_handle=False):
        return filename

    def analyse_monomers(self):
        if self.sequence is not None:
            nodes = self.sequence["internaltype"]["nodes"]
            analysed_nodes = {}
            for node in (set(nodes)):
                analysed_nodes[node] = str(nodes.count(node))
            jsond = []
            for node in analysed_nodes.keys():
                jsond.append(Glycan.jsonitup(node, analysed_nodes[node]))
            # print jsond
            return jsond
        else:
            return None

    @staticmethod
    def jsonitup(ident, value):
        jsonobject = {
            'label': ident,
            'value': value
        }
        return jsonobject

    def analyse_linkages(self):
        if self.sequence is not None:
            links = self.sequence["internaltype"]["edges"].keys()
            analysed_links = {}
            # first split the links for KEGG
            splitlinks = []
            for link in (set(links)):
                splitlink = link.split(':')  # need the last part of the link
                splitlinks.append(splitlink[1])

            for link in (set(splitlinks)):
                analysed_links[link] = str(splitlinks.count(link))
            jsond = []
            for link in analysed_links.keys():
                jsond.append(Glycan.jsonitup(link, analysed_links[link]))
            # return analysed_links
            return jsond
        else:
            return None

    @staticmethod
    def jsonify(filetype, numnodes, numedges, nodes, edges):
        jsonobject = {
            'filetype': filetype,
            'numnodes': numnodes,
            'numedges': numedges,
            'nodes': nodes,
            'edges': edges
        }
        return jsonobject

    def write(self):
        if self.sequence is not None:  # repeated code
            return self  # cheat
        else:
            return None

    @property
    def sequence(self):
        """


        :return:
        """
        return self._sequence

    @sequence.setter
    def sequence(self, value):
        self._sequence = value

    def analytics(self):
        import json

        nodejson = [self.analytify(self.sequence["sourcename"], self.sequence["internaltype"]['numnodes'],
                                   self.sequence["internaltype"]['numedges'], self.sequence["internaltype"]['nodes'][0],
                                   self.analyse_monomers(), self.analyse_linkages())]

        d2 = {"data": [key for key in nodejson]}
        return json.dumps(d2, indent=4)

    def pretty_analytics(self):
        """
        return html viewable form of analytics data
        """
        return self

    def __eq__(self, other):
        if self.sequence["sourcetype"] != 'kcf' or other.sequence["sourcetype"] != 'kcf':
            return False

        if self.sequence["internaltypedata"].is_equal(other.sequence["internaltypedata"]):
            return True
        else:
            return False

    def __hash__(self):
        if self.sequence["sourcetype"] != 'kcf':
            return 0
        else:
            return self.sequence["internaltypedata"].hash()

    @staticmethod
    def analytify(ident, numnodes, numlinkages, root, nodes, linkages):
        jsonobject = {
            'id': ident,
            'numnodes': numnodes,
            'numlinkages': numlinkages,
            'root': root,
            'nodes': nodes,
            'linkages': linkages
        }
        return jsonobject
