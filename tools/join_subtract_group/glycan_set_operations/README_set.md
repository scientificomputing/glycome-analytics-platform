[TOC]

# glycan_sets.py
This tool reads two sets of glycans and return their union, intersection and difference.
At present works best with kcf data.


## Works with Galaxy?
Yes. see [glycan_sets.xml](glycan_sets.xml)

## Command line usage

```
../../virtualpy/bin/activate
python glycan_sets.py --setA $inputa --setB $inputb --uniqueA=$outputa --uniqueB=$outputb --union=$outputu --intersect=$outputi --differenceAB=$outputab --differenceBA=$outputba
```


## Help

```
../../virtualpy/bin/activate
python glycan_sets.py -h
```

## Unit Testing?
Yes.

```
../../virtualpy/bin/activate
python  test_sets.py
```
