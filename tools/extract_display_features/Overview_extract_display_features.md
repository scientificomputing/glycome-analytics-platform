[TOC]

# Overview of feature analysis tools

Tools to analyse features of glycans.

## Resource for Informatics of Glycomes at Soka (RINGS)

###  Glycan Miner
Mine common glycan substructures. [See here](rings_miner/README_miner.md)

### Multiple Carbohydrate Alignment with Weights (MCAW)
Align multiple glycans. [See here](rings_mcaw/README_mcaw.md)

### Fingerprinter
Provide a linear binary based fingerprint based on the motifs found in the glycan.
[see here](rings_fingerprinter/README_fingerprinter.md)

## Other resources

### Fingerprinter dendrogram to image
This tool creates a clade-o-gram. It reads trees in newick-like format, converts to newick and then plots with ETE2.
[see here](draw_fingerprinter_dendrogram/README_drawgram.md)

**Note has a dependency that cannot be met in a virtualenv**


### PKCF to image
Redraw MCAW image. Specify resolution etc. Needs to be added. Currently on [Bitbucket](https://bitbucket.org/chrisbarnettster/pkcftoimage)

**Note has a dependency that cannot be met in a virtualenv**


### Dashboard
**Not usable**
Attempt at incorporating JavaScript and d3 apps into Galaxy.
Could be cool. [See here](dashboard/README_dash.md)

