__license__ = "MIT"

import unittest
import post_fingerprinter as fp


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        self.kcinput = {"rings": """ENTRY       G00015                      Glycan
NODE        8
            1   Asn        20     0
            2   GlcNAc     12     0
            3   GlcNAc      3     0
            4   Man        -5     0
            5   Man       -12     5
            6   Man       -12    -5
            7   GlcNAc    -20     5
            8   GlcNAc    -20    -5
EDGE        7
            1     2:b1    1
            2     3:b1    2:4
            3     4:b1    3:4
            4     5:a1    4:6
            5     6:a1    4:3
            6     7:b1    5:2
            7     8:b1    6:2
///
ENTRY       G00016                      Glycan
NODE        9
            1   Asn        20     3
            2   GlcNAc     12     3
            3   LFuc        4     8
            4   GlcNAc      3    -2
            5   Man        -5    -2
            6   Man       -12     3
            7   Man       -12    -7
            8   GlcNAc    -20     3
            9   GlcNAc    -20    -7
EDGE        8
            1     2:b1    1
            2     3:a1    2:6
            3     4:b1    2:4
            4     5:b1    4:4
            5     6:a1    5:6
            6     7:a1    5:3
            7     8:b1    6:2
            8     9:b1    7:2
///
ENTRY       G00017                      Glycan
NODE        11
            1   Asn        24     3
            2   GlcNAc     14     3
            3   LFuc        7     8
            4   GlcNAc      6    -2
            5   Man        -2    -2
            6   Man        -8     3
            7   Man        -8    -7
            8   GlcNAc    -16     3
            9   GlcNAc    -16    -7
            10  Gal       -24     3
            11  Gal       -24    -7
EDGE        10
            1     2:b1    1
            2     3:a1    2:6
            3     4:b1    2:4
            4     5:b1    4:4
            5     6:a1    5:6
            6     7:a1    5:3
            7     8:b1    6:2
            8     9:b1    7:2
            9    10:b1    8:4
            10   11:b1    9:4
///
ENTRY       G00018                      Glycan
NODE        13
            1   Asn        28     3
            2   GlcNAc     18     3
            3   LFuc       10     8
            4   GlcNAc      9    -2
            5   Man         1    -2
            6   Man        -5     4
            7   Man        -5    -8
            8   GlcNAc    -13     4
            9   GlcNAc    -13    -8
            10  Gal       -21     4
            11  Gal       -21    -8
            12  Neu5Ac    -29     4
            13  Neu5Ac    -29    -8
EDGE        12
            1     2:b1    1
            2     3:a1    2:6
            3     4:b1    2:4
            4     5:b1    4:4
            5     6:a1    5:6
            6     7:a1    5:3
            7     8:b1    6:2
            8     9:b1    7:2
            9    10:b1    8:4
            10   11:b1    9:4
            11   12:a2   10:6
            12   13:a2   11:6
///
ENTRY       G00019                      Glycan
NODE        9
            1   Asn        20     0
            2   GlcNAc     12     0
            3   GlcNAc      3     0
            4   Man        -5     0
            5   Man       -12     5
            6   Man       -12    -5
            7   GlcNAc    -15     0
            8   GlcNAc    -20     5
            9   GlcNAc    -20    -5
EDGE        8
            1     2:b1    1
            2     3:b1    2:4
            3     4:b1    3:4
            4     5:a1    4:6
            5     6:a1    4:3
            6     7:b1    4:4
            7     8:b1    5:2
            8     9:b1    6:2
///
ENTRY       G00020                      Glycan
NODE        9
            1   Asn        20     3
            2   GlcNAc     11     3
            3   GlcNAc      2     3
            4   Man        -6     3
            5   Man       -13     9
            6   Man       -13    -3
            7   GlcNAc    -21     9
            8   GlcNAc    -21     2
            9   GlcNAc    -21    -8
EDGE        8
            1     2:b1    1
            2     3:b1    2:4
            3     4:b1    3:4
            4     5:a1    4:6
            5     6:a1    4:3
            6     7:b1    5:2
            7     8:b1    6:4
            8     9:b1    6:2
///
"""
        }
        self.similaritymatrix = {
            "rings": """G00015\t 1.000000\t 0.777778\t 0.460526\t 0.304348\t 0.564516\t 0.636364\t \nG00016\t 0.777778\t 1.000000\t 0.592105\t 0.391304\t 0.486111\t 0.538462\t \nG00017\t 0.460526\t 0.592105\t 1.000000\t 0.660870\t 0.339806\t 0.378947\t \nG00018\t 0.304348\t 0.391304\t 0.660870\t 1.000000\t 0.255319\t 0.268657\t \nG00019\t 0.564516\t 0.486111\t 0.339806\t 0.255319\t 1.000000\t 0.444444\t \nG00020\t 0.636364\t 0.538462\t 0.378947\t 0.268657\t 0.444444\t 1.000000\t """
        }
        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_kcf_input(self):
        import StringIO

        kchandle = StringIO.StringIO(''.join(self.kcinput["rings"]))
        h = fp.post_rings_fingerprinter(kchandle)
        # print h
        d = fp.extract_dendro_from_fingerprinter_output(h)
        # print d
        self.assertIn(self.similaritymatrix["rings"], h)
        self.assertIn("|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||", h)
        self.assertIn("|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||", d)

    def test_empty_stream(self):
        with self.assertRaises(IOError):
            fp.post_rings_fingerprinter(None)
        with self.assertRaises(IOError):
            fp.post_rings_fingerprinter([])
        with self.assertRaises(IOError):
            fp.post_rings_fingerprinter("")
        with self.assertRaises(IOError):
            fp.extract_dendro_from_fingerprinter_output(None)
        with self.assertRaises(IOError):
            fp.extract_dendro_from_fingerprinter_output([])
        with self.assertRaises(IOError):
            fp.extract_dendro_from_fingerprinter_output("")


def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()
