__author__ = "Chris Barnett"
__version__ = "0.2"
__license__ = "MIT"


def post_rings_fingerprinter(instream, kcf_or_fingerprint=0, glycan_fragment_size=10000, fingerprint_size=1024,
                             number_of_folds=0, calc_tanimoto_or_modifiedt=1, show_unitfingerprint=0,
                             show_groupfingerprint=0, groupfingerprintname="fingerprint", show_similarity_matrix=1,
                             show_dissimilarity_matrix=0, show_mode=0):
    """
    :param instream: instream (kcf or fingerprint) to be placed in the form text area
	* input data format : <input type="radio" name="format" id="form1" value="0" checked><label for="form1">KCF</label>
				<input type="radio" name="format" id="form2" value="1"><label for="form2">Glycan Fingerprint</label>
				<br><p>
	* glycan fragment size : <input type="text" name="frag" size="5" value = "10000"> <br>
        * fingerprint size : <input type="text" name ="size" size="15" value = "1024"> <br>
        * number of folds : <input type="text" name ="folds" size="5" value ="0"> <br>
	* calc type : <input type="radio" name ="calc" id ="c1" value="1" checked><label for="c1">Tanimoto</label>
		     <input type="radio" name ="calc" id ="c2" value="2"><label for="c2">Modified tanimoto</label>
        show unit fingerprint : <input type="radio" name ="fprint" id ="f1" value="1"><label for="f1">On</label>
                                <input type="radio" name ="fprint" id ="f2" value="0" checked><label for="f2">Off</label>
	show group fingerprint : <input type="radio" name ="sprint" id ="s1" value="1"><label for="s1">On</label>
                                 <input type="radio" name ="sprint" id ="s2" value="0" checked><label for="s2">Off</label>
	      groupe fingerprint name : <input type="text" name="gf_name" size="20" value="fingerprint"><br>
	show similarity matrix : <input type="radio" name ="sim_mat" id ="sm1" value="1" checked><label for="sm1">On</label>
                                 <input type="radio" name ="sim_mat" id ="sm2" value="0"><label for="sm2">Off</label>
	show dissimilarity matrix : <input type="radio" name ="dis_mat" id ="dm1" value="1"><label for="dm1">On</label>
	                            <input type="radio" name ="dis_mat" id ="dm2" value="0" checked><label for="dm2">Off</label>
	groupe fingerprint mode : <input type="radio" name ="show_mode" id ="s_mode1" value="1"><label for="s_mode1">On</label>
				  <input type="radio" name ="show_mode" id ="s_mode2" value="0" checked><label for="s_mode2">Off</label>
    """
    import urllib2, urllib

    if instream is None or instream == [] or instream == "":
        raise IOError("empty input stream")

    # URL user would navigate to rings.t.soka.ac.jp/cgi-bin/tools/Fingerprint2/Fingerprint_index.pl
    # URL to post to
    url = 'http://rings.t.soka.ac.jp/cgi-bin/tools/Fingerprint2/run_fingerprint1-2.pl'
    kcfdata = instream.read()
    file = ""
    # values contains all the names of the items in the form and the appropriate data
    values = {'datasetname': 'default', 'KCF': kcfdata, 'KCFFILE': file, 'format': kcf_or_fingerprint,
              'frag': glycan_fragment_size, 'size': fingerprint_size, 'folds': number_of_folds,
              'calc': calc_tanimoto_or_modifiedt, 'fprint': show_unitfingerprint, 'sprint': show_groupfingerprint,
              'gf_name': groupfingerprintname, 'sim_mat': show_similarity_matrix, 'dis_mat': show_dissimilarity_matrix,
              'show_mode': show_mode, 'submit': 'SUBMIT'}

    html = urllib2.urlopen(url, urllib.urlencode(values)).readlines()

    if html is None or str(html).strip() == "":
        return None
    for line in list(html):  # hack to remove unable to open file message
        if "<title" in line or "</title" in line:
            html.remove(line)
        elif "<h1" in line or "</h1" in line:
            html.remove(line)
        elif "Content-type: text/html" in line:
            html.remove(line)
        elif "Error" in line:
            html.remove(line)
    return ''.join(html[:])


def extract_dendro_from_fingerprinter_output(html):
    """

    :param html: output from fingerprinter
    :return: dendrogram string
    """
    if html is None or html == [] or html == "":
        raise IOError("empty html stream")
    clusterinfo = ""  # preassigned just in case
    for line in html.split('\n'):
        if "Last Cluster" in line:
            clusterinfo = (line.strip()).split(":")[1].lstrip()
    return clusterinfo


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", action="store", type="string", dest="i", default="input.kcf",
                      help="input kcf file or fingerprint (input)")
    parser.add_option("-o", action="store", type="string", dest="o", default="output.html",
                      help="output html file (output)")
    parser.add_option("-d", action="store", type="string", dest="d", default="output.dendrogram",
                      help="dendrogram text output")
    parser.add_option("-f", action="store", type="int", dest="f", default=0,
                      help="kcf (0) or fingerprint (1) format ")
    parser.add_option("-g", action="store", type="int", dest="g", default=10000,
                      help="glycan fragment size ")
    parser.add_option("-s", action="store", type="int", dest="s", default=1024,
                      help="fingerprint size ")
    parser.add_option("-n", action="store", type="int", dest="n", default=0,
                      help="number of folds")
    parser.add_option("-c", action="store", type="int", dest="c", default=1,
                      help="calc type - Tanimoto (1) or Modified Tanimoto (2)")
    parser.add_option("-u", action="store", type="int", dest="u", default=0,
                      help="show unit fingerprint")
    parser.add_option("-r", action="store", type="int", dest="r", default=0,
                      help="show group fingerprint")
    parser.add_option("-a", action="store", type="string", dest="a", default="fingerprint",
                      help="group fingerprint name")
    parser.add_option("-m", action="store", type="int", dest="m", default=1,
                      help="show similarity matrix")
    parser.add_option("-l", action="store", type="int", dest="l", default=0,
                      help="show dissimilarity matrix")
    parser.add_option("-e", action="store", type="int", dest="e", default=0,
                      help="show mode")

    (options, args) = parser.parse_args()
    try:
        inputname = options.i
        outputname = options.o
        outputdendro = options.d
    except Exception as e:
        raise Exception(e, "Please pass an input (kcf), output filename and dendrogram out filename as arguments")
    instream = file(inputname, 'r')

    try:
        html = post_rings_fingerprinter(instream, kcf_or_fingerprint=options.f, glycan_fragment_size=options.g,
                                        fingerprint_size=options.s,
                                        number_of_folds=options.n, calc_tanimoto_or_modifiedt=options.c,
                                        show_unitfingerprint=options.u,
                                        show_groupfingerprint=options.r, groupfingerprintname=options.a,
                                        show_similarity_matrix=options.m,
                                        show_dissimilarity_matrix=options.l, show_mode=options.e)
        with open(outputname, "w") as f:
            f.write(html)
        with open(outputdendro, "w") as f:
            f.write(extract_dendro_from_fingerprinter_output(html))
    except Exception as e:
        raise

