
[TOC]

# post_fingerprinter.py
This tool reads kcf glycans and uses the RINGS fingerprint to provide a linear binary based fingerprint based on the motifs found in the glycan.

## Works with Galaxy?
Yes. see [post_fingerprinter.xml](post_fingerprinter.xml)


## Command line usage
```
../../virtualpy/bin/activate
python post_fingerprinter.py -i $input -o $output -d $dendrogram -f $format -g $glycanfragmentsize -s $fingerprintsize -n $folds -c $tanimoto -u $showunit -r $showgroup -m $showsimmatrix -l $showdissimmatrix -e $showmode
```

## Help
```
../../virtualpy/bin/activate
python post_fingerprinter.py -h
```

## Unit Testing?
Yes. Use test_post_fingerprinter.py

```
../../virtualpy/bin/activate
python test_post_fingerprinter.py
```
