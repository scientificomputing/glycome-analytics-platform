__license__ = "MIT"
__version__ = "0.2"

def post_rings_miner(instream, alpha=1, minsup=1):
    """

    :param instream: kcf to be passed to the textarea
    :param alpha: alpha parameter between 0 and 1
    :param minsup: minimum number by which the resulting substructure should appear should be specified for minimum support
    """
    import urllib2
    import urllib

    if instream is None or instream == [] or instream == "":
        raise IOError("input stream is empty")

    # URL that users would navigate to rings.t.soka.ac.jp/cgi-bin/tools/GlycanMiner/Miner_index.pl
    # URL to post to
    url = 'http://rings.t.soka.ac.jp/cgi-bin/tools/GlycanMiner/miner.pl'
    kcfdata = instream.read()
    file = ""
    # values contains all the names of the items in the form and the appropriate data
    values = dict(datasetname='default', KCF=kcfdata, KCFFILE=file, alpha=alpha, minsup=minsup, submit='SUBMIT')
    html = urllib2.urlopen(url, urllib.urlencode(values)).readlines()

    if html is None or str(html).strip() == "":
        return None

    for line in list(html):  # hack to remove unable to open file message
        if "<title" in line or "</title" in line:
            html.remove(line)
        elif "<h1" in line or "</h1" in line:
            html.remove(line)
        elif "Error" in line:
            html.remove(line)
    return ''.join(html[2:])
    # output will seem empty if input issues, otherwise returns embedded images


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", action="store", type="string", dest="i", default="input.kcf",
                      help="input kcf file (input)")
    parser.add_option("-o", action="store", type="string", dest="o", default="output.html",
                      help="output html file from miner (output)")
    parser.add_option("-a", action="store", type="int", dest="a", default="1",
                      help="alpha value (between 0 and 1)")
    parser.add_option("-m", action="store", type="int", dest="m", default="1",
                      help="minimum support")

    (options, args) = parser.parse_args()

    try:
        inputname = options.i
        outputname = options.o
    except Exception as e:
        raise Exception(e, "Please pass an input (kcf), output html  as arguments")
    instream = file(inputname, 'r')

    try:
        html = post_rings_miner(instream, alpha=options.a, minsup=options.m)
        with open(outputname, "w") as out:
            out.write(html)
    except Exception as e:
        raise e
