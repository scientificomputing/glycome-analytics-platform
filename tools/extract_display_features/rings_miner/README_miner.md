
[TOC]

# post_miner.py
This tool reads kcf glycans and mines them for common subgraphs using the RINGS.


## Works with Galaxy?
Yes. see [post_miner.xml](post_miner.xml)


## Command line usage
```
../../virtualpy/bin/activate
python post_miner.py -i $input -o $output -a $alpha -m $minsup
```

## Help
```
../../virtualpy/bin/activate
python post_miner.py -h
```

## Unit Testing?
Yes. Use test_post_miner.py

```
../../virtualpy/bin/activate
python test_post_miner.py
```

