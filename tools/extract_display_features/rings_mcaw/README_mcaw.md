
[TOC]

# post_mcaw.py
This tool uses read glycans in kcf format and aligns them using the RINGS MCAW tool.

## Works with Galaxy?
Yes. see [post_mcaw.xml](post_mcaw.xml)


## Command line usage
```
../../virtualpy/bin/activate
python post_mcaw.py -i $input -o $htmloutput -p $pngoutput -k $pkcfoutput -g $gap -s $sugar -a $anomer -n $nonreducing -r $reducing
```

## Help
```
../../virtualpy/bin/activate
python post_mcaw.py -h
```

## Unit Testing?
Yes. Use test_post_mcaw.py

```
../../virtualpy/bin/activate
python test_post_mcaw.py
```
