__license__ = "MIT"

import unittest
import draw_gram as dg


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_empty_stream(self):
        with self.assertRaises(IOError):
            dg.parse_finger_to_newick(None)
        with self.assertRaises(IOError):
            dg.parse_finger_to_newick([])
        with self.assertRaises(IOError):
            dg.parse_finger_to_newick("")

    def test_basic_input(self):
        input_from_fingerprinter = "|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||"
        output = dg.parse_finger_to_newick(input_from_fingerprinter)
        self.assertEqual(output, "(((G00015,G00016),(G00019,G00020)),(G00017,G00018))")

    def test_incorrect_input(self):
        input_from_fingerprinter = "A|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||"
        with self.assertRaises(TypeError):
            dg.parse_finger_to_newick(input_from_fingerprinter)
        input_from_fingerprinter = "|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||B"
        with self.assertRaises(TypeError):
            dg.parse_finger_to_newick(input_from_fingerprinter)
        input_from_fingerprinter = "A|B"
        with self.assertRaises(TypeError):
            dg.parse_finger_to_newick(input_from_fingerprinter)

    def test_uneven_brackets(self):
        input_from_fingerprinter = "||G00015_G00016|_|G00019_G00020||_|G00017_G00018||"
        with self.assertRaisesRegexp(TypeError, "uneven"):
            dg.parse_finger_to_newick(input_from_fingerprinter)

    def test_render_tree(self):
        from ete2 import Tree
        import os

        try:
            os.remove("output.png")
            os.remove("output.svg")
        except:
            pass
        input_from_fingerprinter = "|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||"
        tree = Tree(dg.parse_finger_to_newick(input_from_fingerprinter) + ";")
        dg.render_tree(tree, "output.png")
        assert os.path.isfile("output.png")
        dg.render_tree(tree, "output.svg")
        assert os.path.isfile("output.svg")

    def test_render_tree_bad_filename(self):
        from ete2 import Tree
        import os

        input_from_fingerprinter = "|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||"
        tree = Tree(dg.parse_finger_to_newick(input_from_fingerprinter) + ";")
        with self.assertRaisesRegexp(TypeError,"Invalid input"):
            dg.render_tree(tree, "output.dat")

    def test_render_tree_mod(self):
        from ete2 import Tree
        import os

        try:
            os.remove("output2.png")
            os.remove("output3.png")
        except:
            pass
        input_from_fingerprinter = "|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||"
        tree = Tree(dg.parse_finger_to_newick(input_from_fingerprinter) + ";")
        dg.render_tree(tree, "output2.png", show_leaves=True, mode="c", arc_start=0, arc_span=180, width=300, dpi=300)
        dg.render_tree(tree, "output3.png", show_leaves=True, mode="r", arc_start=0, arc_span=180, width=1100, dpi=300)
        assert os.path.isfile("output2.png")
        assert os.path.isfile("output3.png")


def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()
