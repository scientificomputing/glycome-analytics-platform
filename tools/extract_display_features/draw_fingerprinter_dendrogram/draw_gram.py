__author__ = "Chris Barnett"
__version__ = "0.2"
__license__ = "MIT"

def rec_sub(string, count, multichar, char):
    """
    recursive substition
    :param string: original string to be parsed for chars to replace
    :param count: times to recurse
    :param multichar: original char
    :param char: replacement char
    :return: modified string
    """
    import re

    if count == 0:
        return string
    repstr = ""  # replacement string pattern
    for i in range(0, count):
        repstr = repstr + char
    repstr = char + repstr + char
    matchstr = "\\" + char + "("
    for i in range(0, count):
        matchstr = matchstr + "\\" + multichar
    matchstr = matchstr + ")\\" + char
    newstring = re.sub(matchstr, repstr, string)
    newstring = rec_sub(newstring, count - 1, multichar, char)
    return newstring


def parse_finger_to_newick(input):
    """
    change fingerprint linear tree format to newick format. handrolled may break.
    :param input: "|||G00015_G00016|_|G00019_G00020||_|G00017_G00018||
    :return: newick string
    """
    import re

    if input is None or input == [] or input == "":
        raise IOError("empty html stream")
    # make sure no leading or trailing spaces:
    input = input.lstrip().rstrip()

    # . the first | is ( and the last | is )
    if input[0] == "|" and input[-1] == "|":
        mod0 = "(" + input[1:-1] + ")"
    else:
        raise TypeError("input does not start and end with | as expected")

    # check if have an even number of brackets. Else return an error
    if input.count('|') % 2 != 0:
        raise TypeError("Invalid input. uneven number of brackets.")

    # . replace |,| with ),(
    mod1 = re.sub("\|_\|", "),(", mod0)

    # . replace text| with text)
    mod2 = re.sub("([a-zA-Z0-9])\|", "\\1)", mod1)

    # . replace |text with (text
    mod3 = re.sub("\|([a-zA-Z0-9])", "(\\1", mod2)

    # . replace )|) with ))) and )|||)  to )))))
    # .  finding max sequential |||| and then create matches based on this
    maxsequence = len(max(input.split('_'), key=len))
    mod4 = rec_sub(mod3, maxsequence, "|", "(")
    mod5 = rec_sub(mod4, maxsequence, "|", ")")
    mod5a = re.sub("\|_", ")_", mod5)

    # . now remove any left over _
    mod6 = re.sub("_", ",", mod5a)

    # .! nope can never do this it is illegal!!
    # <illegal code>
    # #. count leftover | and replace the first half with (
    # print mod6, mod6.count('|')/2
    # mod7= re.sub("\|","(",mod6,count=mod6.count('|')/2)
    # print mod7
    # #. count leftover | and replace the first half with )
    # mod8= re.sub("\|",")",mod7,count=mod6.count('|')/2)
    # print mod8
    # </illegal code>

    # . should be no leftover | .
    if '|' in mod6:
        print "leftover |'s in ", mod6
        raise BaseException
    return mod6


def render_tree(tree, outputname, show_leaves=True, mode="c", arc_start=-180, arc_span=180, width=1000, dpi=600):
    try:
        from ete2 import TreeStyle
    except ImportError:
        raise ImportError("ete2 TreeStyle module not loaded properly, check dependencies especially PyQt4")

    if ".png" not in outputname:
        if ".svg" not in outputname:
            raise TypeError(outputname + "Invalid input. Must be  *.png or *.svg")

    ts = TreeStyle()
    ts.show_leaf_name = show_leaves
    ts.mode = mode
    ts.arc_start = arc_start  # 0 degrees = 3 o'clock
    ts.arc_span = arc_span
    tree.render(outputname, w=width, tree_style=ts, dpi=dpi)


if __name__ == "__main__":
    try:
        from ete2 import Tree
    except ImportError:
        raise ImportError("ete2 Tree module not loaded properly, check dependencies")
    try:
        from ete2 import TreeStyle
    except ImportError:
        raise ImportError("ete2 TreeStyle module not loaded properly, check dependencies especially PyQt4")
    import os, sys
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", action="store", type="string", dest="i", default="input",
                      help="input file in linear dendrogram format")
    parser.add_option("-o", action="store", type="string", dest="o", default="output.png",
                      help="output png or svg file. suffix determines format  (output.png)")

    (options, args) = parser.parse_args()

    try:
        inputname = options.i
        outputname = options.o
    except Exception as e:
        raise Exception(e, "Please pass an input (kcf), output filename as arguments")
    instream = file(inputname, 'r')

    try:
        t = Tree(parse_finger_to_newick(instream.readline()) + ";")
    except Exception as e:
        raise e

    render_tree(t, outputname)

    # if called from Galaxy I get a filename like file.dat.png or file.dat.svg
    # but galaxy thinks it is called filename.dat so must rename
    if "dat" in outputname:
        spli = outputname.split('.')
        os.system("mv " + outputname + " " + ".".join([spli[0], spli[1]]))
