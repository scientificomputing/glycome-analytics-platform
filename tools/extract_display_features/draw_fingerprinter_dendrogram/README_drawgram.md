
[TOC]

# draw_gram.py
This tool creates a clade-o-gram. It reads trees in newick-like format, converts to newick and then plots with ETE2.

## Works with Galaxy?
Yes, but has requirements that cannot be installed in a virtual env.
See [draw_gram.xml](draw_gram.xml)

## Requirements

This tool requires ete2 and ete2 requires PyQt4. PyQt4 is not installable via pip or easy_install at present.
Without PyQt4, ete2.TreeStyle does not load and images cannot be produced.

### Workaround for PyQt4

#### For RHEL6.5, using python2.7 via puias repo

Install PyQt4 and PyQt4-devel using your distributions install tool.

```
yum install PyQt427-devel.x86_64 PyQt427.x86_64
```

this will also install sip.

Confirm that your systemwide python can import PyQt4 (if not confirm your install)

Then copy these from the system libraries to your virtualenv

```
cp -r  /usr/lib64/python2.7/site-packages/sip* virtualpy2.7/lib64/python2.7/site-packages
cp -r  /usr/lib64/python2.7/site-packages/PyQt4 virtualpy2.7/lib64/python2.7/site-packages
```

You should be able to import PyQt4 and TreeStyle with no errors

```
virtualpy2.7/bin/python
import PyQt4
from ete2 import TreeStyle
```



## Command line usage
```
../../virtualpy/bin/activate
python draw_gram.py -i $input -o ${pngoutput}.$imagetype
```

## Help
```
../../virtualpy/bin/activate
python draw_gram.py -h
```

## Unit Testing?
Yes. Use test_draw_gram.py

```
../../virtualpy/bin/activate
python test_draw_gram.py
```
