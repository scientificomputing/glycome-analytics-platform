[TOC]

# lc_extend.py
This tool provides all permutations of linear code with uncertain leaves.

*Buggy, not guaranteed to function properly...*

## Works with Galaxy?
Yes. see [lc_extend.xml](lc_extend.xml)

## Command line usage

```
../../virtualpy/bin/activate
python lc_extend.py -i $input -o $output -l $logfile $appendorextend
```

## Example input / output
Query sequence

```
(Ma2Ma3(Ma3(Ma6)Ma6)Mb4GNb4GN,M)
(A??GN??Ma3(GN??Ma6)Mb4GNb4(Fa6)GN)
```

Default Output sequence

```
((M??)X??)(Ma2Ma3(Ma3)(Ma6)Ma6)Mb4GNb4GN
(A??GN??Ma3(GN??Ma6)Mb4GNb4(Fa6)GN)
```

Extended Output sequence _(not recommended)_

```
M??Ma2Ma3(Ma3(Ma6)Ma6)Mb4GNb4GN
Ma2Ma3(M??Ma3(Ma6)Ma6)Mb4GNb4GN
Ma2Ma3(Ma3(M??Ma6)Ma6)Mb4GNb4GN
(A??GN??Ma3(GN??Ma6)Mb4GNb4(Fa6)GN)
```

## Help
```
../../virtualpy/bin/activate
python lc_extend.py -h
```

## Unit Testing?
Yes.


```
../../virtualpy/bin/activate
python  test_lc_extend.py
```
