__license__ = "MIT"

import unittest
import rename_kcf as rk


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_simple_kcf(self):
        import StringIO

        kcinput = """ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = rk.read_meta_kcf(kchandle)
        i = rk.flatten_meta_kcf_list(h)
        self.assertIn("ENTRY      GLY0    Glycan\n", i)

    def test_simple_kcf_rename_newcount(self):
        import StringIO

        kcinput = """ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = rk.read_meta_kcf(kchandle, "FLY", 15)
        i = rk.flatten_meta_kcf_list(h)
        self.assertIn("ENTRY      FLY15    Glycan\n", i)

    def test_initial_newline_simple_kcf(self):
        import StringIO

        kcinput = """
ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        h = rk.read_meta_kcf(kchandle)
        i = rk.flatten_meta_kcf_list(h)
        self.assertIn("ENTRY      GLY0    Glycan\n", i)

    def test_multiple_kcf(self):
        import StringIO

        kcinput = """ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
ENTRY       G00005                      Glycan
NODE        6
            1   PP-Dol     15     1
            2   GlcNAc      8     1
            3   GlcNAc      0     1
            4   Man        -9     1
            5   Man       -16     7
            6   Man       -16    -6
EDGE        5
            1     2:a1    1
            2     3:b1    2:4
            3     4:b1    3:4
            4     5:a1    4:6
            5     6:a1    4:3
///
"""
        kchandle = StringIO.StringIO(''.join(kcinput))
        h = rk.read_meta_kcf(kchandle)
        i = rk.flatten_meta_kcf_list(h)
        self.assertIn("ENTRY      GLY0    Glycan\n", i)
        self.assertIn("ENTRY      GLY1    Glycan\n", i)
        self.assertIn("ENTRY      GLY2    Glycan\n", i)

    def test_empty_stream(self):
        with self.assertRaises(IOError):
            m = rk.read_meta_kcf(None)
        with self.assertRaises(IOError):
            m = rk.read_meta_kcf([])
        with self.assertRaises(IOError):
            m = rk.read_meta_kcf("")

    def test_non_integer_counter(self):
        import StringIO

        kcinput = """ENTRY      ((A??)AN)     Glycan
NODE        2
            1     galnac     0     0
            2     gal     -8     0
EDGE        1
            1     2:1     1
///
"""

        kchandle = StringIO.StringIO(''.join(kcinput))
        with self.assertRaises(TypeError):
            h = rk.read_meta_kcf(kchandle, "FLY", "abc")


def run_tests():
    unittest.main()


if __name__ == '__main__':
    run_tests()


