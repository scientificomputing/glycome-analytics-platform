[TOC]

# rename_kcf.py
This tool reads kcf glycans and sequentially renames them as GLY{$COUNT}. Why? Sometimes entry names have illegal chars e.g. "-" or sometimes they are linearcode.

## Works with Galaxy?
Yes. see [rename_kcf.xml](rename_kcf.xml)

## Command line usage

```
../../virtualpy/bin/activate
python rename_kcf.py -i $input -o $kcfoutput -p $prefix -c $counter
```


## Help
```
../../virtualpy/bin/activate
python rename_kcf.py -h
```

## Unit Testing?
Yes.



```
../../virtualpy/bin/activate
python  test_rename_kcf.py
```
