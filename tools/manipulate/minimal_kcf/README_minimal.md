[TOC]

# rename_kcf.py
This tool reads kcf glycans and removes all additional data which may cause tools to malfunction. For instance REFERENCE, DBLINKS, MASS etc. Only ENTRY, NODE and EDGE entries are kept

## Works with Galaxy?
Yes. see [minimal_kcf.xml](minimal_kcf.xml)

## Command line usage

```
../../virtualpy/bin/activate
python minimal_kcf.py -i $input -o $kcfoutput
```


## Help
```
../../virtualpy/bin/activate
python minimal_kcf.py -h
```

## Unit Testing?
Yes.


```
../../virtualpy/bin/activate
python  test_minimal_kcf.py
```
