__author__ = "Chris Barnett"
__version__ = "0.3"
__license__ = "MIT"


def read_meta_kcf(inputstream):
    """
    :param inputstream: the kcf file
    read kcf file (which may contain multiple kcf entries) and only keep ENTRY, NODE and EDGE parts.
    :return:
    """
    if inputstream is None or inputstream == [] or inputstream == "":
        raise IOError("empty input stream")
    list_of_kcf_paragraphs = []
    kcfpara = None
    for line in inputstream:
        if "ENTRY" in line:
            kcfpara = [line]
        elif "NODE" in line:
            _, totalnodes = line.split()
            totalnodes = int(totalnodes)
            kcfpara.append(line)
            for inodes in range(0, totalnodes):
                nodeline = inputstream.next()
                kcfpara.append(nodeline)
        elif "EDGE" in line:
            _, totaledges = line.split()
            kcfpara.append(line)
            totaledges = int(totaledges)
            for inodes in range(0, totaledges):
                edgeline = inputstream.next()
                kcfpara.append(edgeline)
        elif "///" in line:
            kcfpara.append(line)
            list_of_kcf_paragraphs.append(kcfpara)
    # . sometimes kcf has no /// or final kcf in many has no ////, so add it
    if kcfpara not in list_of_kcf_paragraphs:
        list_of_kcf_paragraphs.append(kcfpara)

    return list_of_kcf_paragraphs  # why this list. easier to deal with each glycan as an individual item in the list


def flatten_meta_kcf_list(metakcflist):
    """

    :param metakcflist:  a list containing lists of strings
    :return: combined kcfs as a large string for saving to file
    """
    import itertools

    return "".join(list(itertools.chain(*metakcflist)))


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-i", action="store", type="string", dest="i", default="input",
                      help="input kcf file (input)")
    parser.add_option("-o", action="store", type="string", dest="o", default="output",
                      help="output kcf file (output)")
    (options, args) = parser.parse_args()

    try:
        inputname = options.i
        outputname = options.o
    except Exception as e:
        raise Exception(e, "Please pass an input (kcf) and output filename as arguments")
    instream = file(inputname, 'r')
    try:
        convertedkcf = read_meta_kcf(instream)
        with open(outputname, "w") as f:
            f.write(flatten_meta_kcf_list(convertedkcf))
    except Exception as e:
        raise e
