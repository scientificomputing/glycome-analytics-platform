[TOC]
# Overview of manipulation tools


Tools to manipulate glycan data.


## Other

### Minimal KCF
[See here for more details](minimal_kcf/README_minimal.md)


### Rename KCF
[See here for more details](rename_kcf/README_rename.md)


### Extend Linearcode

**Incomplete.**

A code that provides all permutations of an unknown glycan.
It can also prepend glycan fragment to the core glycan, this was thought to be possibly useful for future analysis.
These fragments are discarded by certain tools yielding multiple copies of a similar core.
[See here for more details](extend_linearcode/README_extend.md)


