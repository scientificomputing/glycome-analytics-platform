__author__ = "Chris Barnett"
__version__ = "0.1.1"
__license__ = "SCRU"

from BeautifulSoup import BeautifulSoup
import mechanize


class PrettifyHandler(mechanize.BaseHandler):
    def http_response(self, request, response):
        if not hasattr(response, "seek"):
            response = mechanize.response_seek_wrapper(response)
        # only use BeautifulSoup if response is html
        if response.info().dict.has_key('content-type') and ('html' in response.info().dict['content-type']):
            soup = BeautifulSoup(response.get_data())
            response.set_data(soup.prettify())
        return response


def mechanise_cfg_array(sample, arraytypeversion, page='http://www.functionalglycomics.org/glycomics/publicdata/primaryscreen.jsp', debug=False):
    """
    Use mechanise to submit input glycan and formats to the new converter tool at RINGS
    """
    import mechanize
    import cookielib

    # create a Browser
    br = mechanize.Browser()
    br.add_handler(PrettifyHandler())

    #  handle cookies - Cookie Jar
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)

    # Browser options
    br.set_handle_equiv(True)
    br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)

    # Follows refresh 0 but not hangs on refresh > 0
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

    # Want debugging messages?
    br.set_debug_http(debug)
    br.set_debug_redirects(debug)
    br.set_debug_responses(debug)

    br.addheaders = [('User-agent',
                      'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    # Open site
    #page = 'http://www.functionalglycomics.org/glycomics/common/jsp/samples/searchSample.jsp?templateKey=2&12=CellType&operation=refine'
    
    br.open(page)

    # Show the response headers
    response_info = br.response().info()

    # select the input form
    br.select_form(nr=0)
#    for form in br.forms():
#        print "Form name:", form.name
#        print form

#Form name: pspresentation
#<pspresentation POST http://www.functionalglycomics.org/glycomics/publicdata/selectedScreens.jsp application/x-www-form-urlencoded
#  <CheckboxControl(animalLectins=[true])>
#    <CheckboxControl(selectedSetsToDisplay=[cTypeLectins, iTypeLectins, sTypeLectins, otherAnimalLectins, plantLectins, antiBodies, other, cellsOrganisms, v1, v2, v3, pv1, pv2, pv21, pv3, pv31, pv32, pv4, pv41, pv42, pv5, pv51, pv52, bactv1])>>
#
    #. select appropriate input on form
    control = br.form.find_control("selectedSetsToDisplay")
    # must select sample and arraytype all at once
    #sample = ["cTypeLectins", "iTypeLectins", "sTypeLectins", "otherAnimalLectins", "plantLectins", "antiBodies", "other", "cellsOrganisms"]
    #arraytypeversion = ["v1", "v2", "v3", "pv1", "pv2", "pv21", "pv3", "pv31", "pv32", "pv4", "pv41", "pv42", "pv5", "pv51", "pv52", "bactv1"]
    #sample = ["cTypeLectins"]
    #arraytypeversion = ["v1"]
    mergedselections = sample + arraytypeversion
    br[control.name] = mergedselections
    #print control
#              <input type="radio" name="dataNature" value="DataOnly"/> Data only
#                          &nbsp;
#                                  <input type="radio" name="dataNature" value="InconclusiveOnly"/> Inconclusive Only
#                                          &nbsp;
#                                                  <input type="radio" name="dataNature" value="All"/>All
#                                                      </td></tr>
    #. cannot select radio button at present. All data will be returned
#    print br.find_control(type="radio")
#    radiocontrol=br.find_control("dataNature")
#    datastatus = "InconclusiveOnly"
#    br[radiocontrol.name] = datastatus
    br.submit()

    response = br.response().read()  # the converted file!

    if response is None or str(response.strip()) == '':
        raise IOError("empty response")
    return response


def clean_json_response(response):
    """
        # look at json  status has failure status, submitData has original data and result has output with format

    :param response: json from RINGS convert
    :return: a list of glycan structures
    """
    import json
    import re
    import StringIO

    # RINGS bug, additional data returned with JSON format. Clean this up
    jsonoutputasfilehandle = StringIO.StringIO(''.join(response))
    keeplines = []  # only keep lines that look like JSON
    for line in jsonoutputasfilehandle.readlines():
        if line[0] == "[":
            keeplines.append(line)

    response2 = ''.join(keeplines)
    # RINGs bug. Now remove junk data appended to the JSON lines for example "}}]GLC "
    p = re.compile('(}}].*)')
    jsontobeparsed = p.subn('}}]', response2)

    # load json
    loaded_as_json = json.loads(jsontobeparsed[0])
    structures = []

    # there could be multiple structures so iterate over, structures are numbered using the No tag.

    # .. matches for  linearcode and eol fixes
    linearcodefix = re.compile('(;$)')
    eolfix = re.compile('(\n$)')

    for glycan in loaded_as_json:
        if str(glycan["status"]) == "false":
            raise IOError(glycan["result"][
                              "message"])  # raise error even though not all structures need be broken. It is no use letting through a broken structure. important to let the user know.
        else:
            # bugfix remove ";" from end of LinearCode
            lcfixed = linearcodefix.subn('\n', str(glycan["result"]["structure"]))[0]
            # now remove all \n at end of each sequence. Some have, some don't, so remove all and add later
            eolfixed = eolfix.subn('', lcfixed)[0]
            structures.append(eolfixed)

    return structures


def get_data_from_soup(html, sitename="http://www.functionalglycomics.org"):
    """
     get msa from soup

    :param html: html from posting to mcaw
    :return: msa
    """
    from BeautifulSoup import BeautifulSoup
    import urllib2
    import re

    if html is None or html == [] or html == "":
        raise IOError("input stream is empty")
    soup = BeautifulSoup(html)
    pkcf = ""  # set to empty
    # tr class="gtTablePresentation"
    #print soup
    all_data = [] # all conclusive data
    inconclusive_data = [] # inconclusive data
    #print len(soup.findAll('tr', {"class": "data"}))
    #print len(soup.findAll('tr', {"class": "data"})[0:4])
    for row in soup.findAll('tr', {"class": "data"}):
        cells = row.findAll("td")
        if len(cells) == 14:  # double check we know what this pages looks like :)
            all_data.append(create_data_entry(cells, sitename,conclusiveness=True))
        else: # we don't know how to parse this so pass
            #print "NOT DATA"  # change this to log
            pass
    #. now find the inconclusive data
    for row in soup.findAll('tr', {"class": "inconclusive"}):
        cells = row.findAll("td")
        if len(cells) == 14:  # double check we know what this pages looks like :)
            inconclusive_data.append(create_data_entry(cells, sitename,conclusiveness=False))
        else: # we don't know how to parse this so pass
            #print "NOT DATA"  # change this to log
            pass

    return all_data,  inconclusive_data


##        #print "THE ONE ", link, "\n"
##        try:
#            #if "gtTablePresentation" in link.get('class'):
##
##                #print "THE ONE ", link, "\n"
###                for descendant in link.descendants:
###
###                    print "EACH ", descendant
#
##                print "CONTENTS: ", link.contents
##                print "CHILDREN: ", link.children
##
###                print link.
##                print "DESCENDANTS: ", link.descendants
##                print "SIBLING: ", link.next_sibling
##                print len(list(link.children))
##                print len(list(link.descendants))
##                print len(list(link.contents))
##
##        except:
##            pass
#
##    for link in soup.findAll("td"):
##        try:
##            if "glycoEnzymeTablePresentation" in link.get('class'):
##                print link.next_sibling.next_sibling.next_sibling
##                #print link.children
##
##        except:
##            pass
#        #if "gtTablePresentation" in link.get('class'):
#        #    print link
##        if "pkcf" in link.get('href'):
##            pkcflink = link.get('href')
##            pkcf = urllib2.urlopen(pkcflink).read()
#
##    for link in soup.findAll(href=re.compile(".*msa")):
##        try:
##            if "msa" in link.get('href'):
##                print link
##        except:
##            pass
#        #print link
#    return


def create_data_entry(cells, sitename,conclusiveness):
    """

    :param cells: list of beautiful soup data parsed from the CFG
    :return: dictionary with cleaned cfg data
    """
    from BeautifulSoup import BeautifulSoup
    try:
        sample=cells[0].find(text=True).strip()
        #print sample
        #. there may be 2 links, so far a duplicate. must be an error
        sampleinformationviewlink= helper_clean_links(cells[0], sitename)
        #print sampleinformationviewlink
        species=cells[1].find(text=True).strip()
        #print species
        proteinfamily=cells[2].find(text=True).strip()
        #print proteinfamily
        investigator=cells[3].find(text=True).strip()
        #print investigator
        #. not always a link
        investigatorlink=helper_clean_links(cells[3], sitename)
        #print investigatorlink
        #. cells 4 is a nested but it's info is repeated in cells 5-7
        experimentarrayview=helper_clean_links(cells[5], sitename)
        #print experimentarrayview
        ean=cells[5].find(text=True)
        if ean == "" or ean is None:
            experimentarraynumber=""
        else:
            experimentarraynumber=cells[5].find(text=True).strip()
        #print experimentarraynumber
        experimentarrayversion=cells[6].find(text=True).strip()
        #print experimentarrayversion
        #. cells [7] is just a spacer

        #. cells[8] is also nested and it's info is repeated in cells 9-13
        #. this may not be true for all data. need to check.
        primaryscreeninfolink=helper_clean_links(cells[9], sitename)
        #print primaryscreeninfolink
        rawdatalink=helper_clean_links(cells[10], sitename)
        #print rawdatalink
        #. cells 11 is a spacer
        rawdatadate=cells[12].find(text=True).strip()
        #print rawdatadate
        rawdatapublicstatus=cells[13].find(text=True).strip()
        #print rawdatapublicstatus



        jsonobject = {
            'sample': sample,
            'sampleinformationviewlink': sampleinformationviewlink,
            'species': species,
            'proteinfamily': proteinfamily,
            'investigator': investigator,
            'investigatorlink': investigatorlink,
            'experimentarrayview': experimentarrayview,
            'experimentarraynumber': experimentarraynumber,
            'experimentarrayversion': experimentarrayversion,
            'primaryscreeninfolink': primaryscreeninfolink,
            'rawdatalink': rawdatalink,
            'rawdatadate': rawdatadate,
            'rawdatapublicstatus': rawdatapublicstatus,
            'conclusive': conclusiveness
        }
        return jsonobject
    except Exception as e:
        print cells
        print "very likely missing data in webpage"
        print e
        raise e


def helper_clean_links(tag, sitename):
    """

    :param list_of_links: list of links to images or mass spec. may be of size 0 - n
    :param sitename: website name to prepend to relative links
    :return:
    """
    from BeautifulSoup import BeautifulSoup
    new_list_of_links = []
    if tag is [] or None:
        return []
    else:
        list_of_links = tag.findAll('a')
        if list_of_links is [] or None:
            return []

        for link in list_of_links:
            thislink=str(link.get("href"))
            if "java" in thislink:
                thislink=thislink[thislink.find("(")+2:thislink.find(")")-1]
            if not("http" in thislink):
                thislink=str(sitename) + thislink

            new_list_of_links.append(thislink)
            #new_list_of_links.append(str(sitename) + str(link.get("href")))

        return new_list_of_links



def helper_write_to_json(data, output, the_indent=4):
    import json
    j2 = json.dumps(data, indent=the_indent)
    f2 = open(output, 'w')
    print >> f2, j2
    f2.close()

def helper_write_to_html(data, output, output_path, options):
    import os
    from jinja2 import Environment, FileSystemLoader
    fileextension=".xls"
    #env = Environment(loader=PackageLoader(os.path.dirname(os.path.abspath(__file__)), 'templates'))
    currentpath = os.path.dirname(os.path.abspath(__file__))
    env = Environment(loader=FileSystemLoader(os.path.join(currentpath, 'templates')),lstrip_blocks=True, trim_blocks=True )
    #env = Environment(FileSystemLoader(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')))
    template = env.get_template('base_array.html')
    
    for entry in data:  # this copied from download_and_save_msa
        if len(entry["rawdatalink"]) == 0:
            entry['href']=""
            #entry['caption']="no xls data found for "+"_".join([entry["sample"], entry["species"], entry["proteinfamily"], entry["investigator"], entry["experimentarraynumber"]])
            entry['caption']="no xls data found for "+"_".join([u'entry["sample"]'.encode('utf-8','ignore'),entry["species"], entry["proteinfamily"], u'entry["investigator"]'.encode('utf-8','ignore'), entry["experimentarraynumber"]])
# .encode('utf-8')   # try encode utf8 to avoid UnicodeEncodeError: 'ascii' codec can't encode character u'\xb0' in position 200622: ordinal not in range(128
        else:
            try:
                objid = entry["rawdatalink"][0][entry["rawdatalink"][0].find("objId")+6:]
                entry['href'] = os.path.join(output_path,"_".join([entry["species"],entry["proteinfamily"], entry["experimentarrayversion"],objid] )+fileextension)
                entry['caption']="_".join([entry["species"], entry["proteinfamily"], u'entry["investigator"]'.encode('utf-8','ignore'), entry["experimentarrayversion"],objid])
                #entry['caption'] =  "_".join([entry["species"],entry["proteinfamily"], entry["experimentarrayversion"],objid] )

            except Exception as e:
                raise e
    with open(output,'w') as f:
        f.write(template.render(title='glycan array output xls summary', items=data,sample=",".join(options_dict["sample"]), arrayversion=",".join(options_dict["arrayversion"]), keepinconclusive=options_dict["keepinconclusive"] ))


def download_and_save_xls(parseddata, filepath):
    """

    :param parseddata: dictionary containing links to msa data
    :return:
    """
    import os
    import urllib2

    fileextension=".xls"
    if not os.path.exists(filepath):
        os.makedirs(filepath)
    for entry in parseddata:
        if len(entry["rawdatalink"]) == 0:
            print "no rawdata for ", entry["sample"], entry["species"], entry["proteinfamily"], entry["investigator"], entry["experimentarraynumber"]
        else:
            try:
                # TODO sanitise sample names and investigator names
                objid = entry["rawdatalink"][0][entry["rawdatalink"][0].find("objId")+6:]
                #. common missing data is as follows
                #.. grep \"\" output.json | grep -v sample | grep -v invest | grep -v number
                #. these items may be designated as not applicable
                #.. grep \"Not output.json | grep -v spec  | grep -v pro
                # error in this analysis sample and inv field are usually full. 
                # but contain nonallphanumeric character sets. do not use as filename.
                # appropriate naming fields include species, proteinfamily, arrayversion, objid

                filename = "_".join([entry["species"],entry["proteinfamily"], entry["experimentarrayversion"],objid] )+fileextension
            except Exception as e:
                raise e

            with open(os.path.join(filepath,filename),'w') as f:
                f.write(urllib2.urlopen(entry["rawdatalink"][0]).read())
        #print entry["rawdata_msa"]
        #print os.path.basename(entry["rawdata_msa"][0])
        #print filename

if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-o", action="store", type="string", dest="o", default="array_output.html",
                      help="html output glycan summary file (output)")
    parser.add_option("--filepath", action="store", type="string", dest="filepath", default="xls_output",
                      help="galaxy file path to be prepended")

    parser.add_option("-j", action="store", type="string", dest="j", default="array_output.json",
                      help="output glycan summary file (output)")
    parser.add_option("-s", action="store", type="string", dest="s", default="all",
                      help="sample type (all)")
    parser.add_option("-g", action="store", type="string", dest="g", default="all",
                      help="glycan array version (all)")
    parser.add_option("-i", action="store_true", dest="i", default=False,
                      help="download inconclusive data")
    parser.add_option("-t", action="store_true", dest="t", default=False,
                      help="parse local file for testing purposes")
    parser.add_option("-d", action="store_true", dest="d", default=False,
                      help="do not download array xls")
    (options, args) = parser.parse_args()


    sampleall = ["cTypeLectins", "iTypeLectins", "sTypeLectins", "otherAnimalLectins", "plantLectins", "antiBodies", "other", "cellsOrganisms"]
    arrayversionall = ["v1", "v2", "v3", "pv1", "pv2", "pv21", "pv3", "pv31", "pv32", "pv4", "pv41", "pv42", "pv5", "pv51", "pv52", "bactv1"]

    if options.s=="all":
        options.s=sampleall
    else:
        #. clean up sample options and confirm they are legitimate
        checksampleoptions=options.s.strip().replace(" ", "").split(",")
        if set(checksampleoptions).issubset(set(sampleall)):
           options.s=list(set(checksampleoptions)) # any duplicates removed
        else:
           raise IOError("sample input invalid")

    if options.g=="all":
        options.g=arrayversionall
    else:
        #. clean up array options and confirm they are legitimate
        checkarrayoptions=options.g.strip().replace(" ", "").split(",")
        #checkarrayoptions=options.g.strip().split(",")
        if set(checkarrayoptions).issubset(set(arrayversionall)):
           options.g=list(set(checkarrayoptions)) # any duplicates removed
        else:
           raise IOError("array version input invalid")

    #. read webpage using selections
    m = mechanise_cfg_array(options.s, options.g)
    #print m

#    testingfile="info/Selected Primary Screens.html"
#    try:
#        instream = file(testingfile, 'r')
#    except Exception as e:
#       raise IOError(e, "the input file specified does not exist. Use -h flag for help")
#    #. read example output webpage
#    m = instream.read()

    #. parse response for all useful data like links to msa
    s = get_data_from_soup(m)
    #exit(0)

    #. write out json of parsed data dictionary
    helper_write_to_json(s[0]+s[1], options.j) # write out inconclusive data regardless

    #. are we downloading the inconclusive data?
    options_dict={"sample":options.s,"arrayversion":options.g,"keepinconclusive":options.i}
    if options.i:
        dataset=s[0]+s[1]
    else:
        dataset=s[0]
    #. write out html to specified dir
    helper_write_to_html(dataset, options.o, options.filepath,options_dict)
    #. download the xls files and save
    if not options.d:
        download_and_save_xls(dataset,options.filepath)


