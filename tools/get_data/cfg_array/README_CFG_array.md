[TOC]

# 1. cfg_get_glycan_array_data.py

Retrieve glycan array data from the CFG.

With focus on retrieving the xls data and also pull the metadata.

## Works with Galaxy?

Not properly tested yet!! The xml in this directory is not correct.

## Command line usage

**NOT RECOMMENDED** This will download all xls and will take a while.
```
../../virtualpy/bin/activate
python cfg_get_glycan_array_data.py
```

**RECOMMENDED** This will get all metadata and not download xls
```
../../virtualpy/bin/activate
python cfg_get_glycan_array_data.py -d
```

This metadata can be viewed using the [CFG glycan array explorer](https://bitbucket.org/rxncor/cfg-data-chart)

## Help
```
../../virtualpy/bin/activate
python cfg_get_glycan_from_cells.py -h
```

## Unit Testing?
Not yet! Although some testing has been done using *.html in ./info and ./templates
Also passed json metadata to [CFG glycan array explorer](https://bitbucket.org/rxncor/cfg-data-chart)

In principle:

```
../../virtualpy/bin/activate
python test_cfg_get_glycan_from_cells.py

```

