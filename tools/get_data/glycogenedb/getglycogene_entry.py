__author__ = 'cbarnett'
__license__ = "MIT"


def helper_get_G(lineentry):
    if "G" in lineentry.upper():
        splitline = lineentry.split()
        for item in splitline:
            if "G" in item:
                return item
    return None

def get_acceptor_keggids(entry):
    """
    :param entry. entry from glycogene db
    :return: txt keggids
    """
    import StringIO
    import xml.etree.cElementTree as ET
    #handle = StringIO.StringIO(''.join(entry))
    handle = StringIO.StringIO(entry)
    tree = ET.parse(handle)
    #print tree.findtext("acceptors")
    root = tree.getroot()
    print root, root.tag, root.attrib
    for elem in root.findall('./gts/acceptors/acceptor_ref'):
        print "acceptors ", elem.tag, elem.attrib
        a=dict(elem.attrib)
        for key in a.keys():
            print key, a[key]


def get_entry_from_glycogene(gene):
    """
    :param gene. Gene name
    :return: xml of entry information
    """
    import urllib2
    #import xml.etree.cElementTree as ET


    #tree = ET.parse()

    uri = 'http://jcggdb.jp/rcmg/wsglycodb/ggdb/entry/'
    #if inputstream is None or inputstream == []:
    #    raise IOError("empty input stream")
    gene = "ST3GAL1"
    try: 
        dbresponse = urllib2.urlopen(uri + gene).read()
    except Exception as e:
        raise urllib2.HTTPError(e.url, e.code, e.msg, e.hdrs, e.fp)
    return dbresponse


if __name__ == "__main__":
    from optparse import OptionParser

#    usage = "usage: python %prog [options]\n"
#    parser = OptionParser(usage=usage)
#    parser.add_option("-i", action="store", type="string", dest="i", default="input",
#                      help="single or double column text file containing GL entries")
#    parser.add_option("-k", action="store", type="string", dest="k", default="kcf.output",
#                      help="kcf output file name")
#    parser.add_option("-d", action="store", type="string", dest="d", default="db.output",
#                      help="KEGG db entry in text format output file name")
#    (options, args) = parser.parse_args()
#    try:
#        instream = file(options.i, 'r')
#    except Exception as e:
#        raise IOError(e, "the input file specified does not exist. Use -h flag for help")
    entry =  get_entry_from_glycogene("ST3GAL1")
    print entry
    #instream = file("xml.xml",'r')
    #entry = instream.read()
    #print entry
    get_acceptor_keggids(entry)
    #out = file("xml.xml",'w')
    #out.write(entry)
    #out.close()
#    try:
#        kcfout = file(options.k, 'w')
#        dbout = file(options.d, 'w')
#    except Exception as e:
#        raise IOError(e, "cannot open output files. -h flag for help")
#
#    kcfout.write("".join(kcf))
#    dbout.write("".join(db))
#    kcfout.close()
#    dbout.close()

