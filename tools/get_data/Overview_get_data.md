
# Overview of data retrieval tools


Tools to access data from online resources.

## Consortium for Functional Glycomics (CFG)
These tools search and retrieve data from the CFG.

### Glycan Profiling data
[See here for more details](cfg/README_CFG.md)

- retrieve glycan profiling data from cells

### Glycan Array data
[See here for more details](cfg_array/README_CFG_array.md)
- retrieve glycan array data


## Kyoto Encyclopaedia of Genes and Genomes

These tools use the [KEGG REST API](http://www.genome.jp/kegg/rest/keggapi.html)

[See here for more details](kegg_glycan/README_KEGG.md)

 - finds entries with matching query keywords or other query data in a given database
 - get – retrieves given database entries
 - link – find related entries by using database cross-references
