__license__ = "MIT"

import unittest
import getkcfKEGG as gk


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_empty_stream(self):
        """ if empty stream then should return None"""
        with self.assertRaises(IOError):
            m = gk.get_kcf_from_kegg(None)
        with self.assertRaises(IOError):
            m = gk.get_kcf_from_kegg([])

    def test_no_matching_entry(self):
        """
        if non G* entry  then return empty lists
        """
        import StringIO
        import urllib2

        glycanindex = "X00092"
        handle = StringIO.StringIO(''.join(glycanindex))
        m, n = gk.get_kcf_from_kegg(handle)
        self.assertEqual(m, [])
        self.assertEqual(n, [])

    def test_malformed_entry(self):
        """
        if malformed entry that looks like "G*" then return HTTPError
        """
        import StringIO
        import urllib2

        glycanindex = "GL00092"
        handle = StringIO.StringIO(''.join(glycanindex))
        with self.assertRaises(urllib2.HTTPError):
            m, n = gk.get_kcf_from_kegg(handle)

    def test_glycan(self):
        """
        """
        import StringIO

        glycanindex = "G00092"
        handle = StringIO.StringIO(''.join(glycanindex))
        m, n = gk.get_kcf_from_kegg(handle)
        self.assertIsNotNone(m)
        self.assertIsNotNone(n)
        self.assertEqual(1, len(m))
        self.assertEqual(1, len(n))

    def test_several_glycans(self):
        """
        """
        import StringIO

        glycanindex = "G00092\nG00091\nG00093"
        handle = StringIO.StringIO(''.join(glycanindex))
        m, n = gk.get_kcf_from_kegg(handle)
        self.assertIsNotNone(m)
        self.assertIsNotNone(n)
        self.assertEqual(3, len(m))
        self.assertEqual(3, len(n))

