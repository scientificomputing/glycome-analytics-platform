
[TOC]

# 1. findKEGG.py
 **find entries with matching query keywords or other query data in a given database**

## Works with Galaxy?
Yes. see [findKEGG.xml](findKEGG.xml)

## Command line usage
```
../../virtualpy/bin/activate
python findKEGG.py
```

This will use the default settings for dbname, searchpattern and output
To specify these :
```
../../virtualpy/bin/activate
findKEGG.py -d $dbname -q $searchpattern -o $output
```

## Known Issues
Yes, the and/or functionality of the KEGG rest API (Release 78.0+/05-05, May 16 ) does not always function as expected. This has been resolved in the code. More details,  [KEGGnotes.md ](KEGGnotes.md)

## Help
```
../../virtualpy/bin/activate
python findKEGG.py -h
```

## Unit Testing?
Yes. Use test_findKEGG.py

```
../../virtualpy/bin/activate
nosetests  test_findKEGG.py
```

# 2. getkcfKEGG,py
 **get – retrieves given database entries _but only for GLYCANS_**


## Works with Galaxy?
Yes. see [getkcfKEGG.xml](getkcfKEGG.xml)

## Command line usage

```
../../virtualpy/bin/activate
cat "G00092" > input
python getkcfKEGG.py
```

This will use the default settings for input, kcfout and dbout
To specify these :
```
../../virtualpy/bin/activate
    getkcfKEGG.py -i $input -k $kcfout -d $dbout
```

## Help
```
../../virtualpy/bin/activate
python getkcfKEGG.py -h
```

## Unit Testing?
Yes. Use test_getkcfKEGG.py

```
../../virtualpy/bin/activate
nosetests  test_getkcfKEGG.py
```

# 3. linkKEGG.py
 **link – find related entries by using database cross-references**

## Works with Galaxy?
Yes. see [linkKEGG.xml](linkKEGG.xml)

## Command line usage
```
../../virtualpy/bin/activate
python linkKEGG.py
```

This will use the default settings for targetdb, source and output
linkKEGG.py -t $targetdb -s $source -o $output
To specify these :
```
../../virtualpy/bin/activate
linkKEGG.py -t $targetdb -s $source -o $output
```

## Help
```
../../virtualpy/bin/activate
python linkKEGG.py -h
```

## Unit Testing?
Yes. Use test_linkKEGG.py

```
../../virtualpy/bin/activate
nosetests  test_linkKEGG.py
```

