
# notes on KEGG behaviour

Tested with  Release 78.0+/05-05, May 16 (see http://rest.kegg.jp/info/genes)

*Recommendation 1 - Do not combine AND with OR queries*
multiple complex queries are not recommended.

*OR's on certain db's e.g. glycan, enzyme do not always return correctly*

## Shiga toxin behaviour test
Using the shiga toxin example from http://www.genome.jp/kegg/rest/keggapi.html
/find/genes/shiga+toxin	  	for keywords "shiga" and "toxin"
/find/genes/"shiga toxin"	  	for keywords "shiga toxin"

results in test-data
 - test-data/shiga-toxin-example-AND
 - test-data/shiga-toxin-example-AND-broken
 - test-data/shiga-toxin-example-OR
 - test-data/shiga-toxin-example-OR-broken


### AND example
/find/genes/shiga+toxin	  	for keywords "shiga" and "toxin"
http://rest.kegg.jp/find/genes/shiga+toxin

### OR example
/find/genes/"shiga toxin"	  	for keywords "shiga toxin"
http://rest.kegg.jp/find/genes/"shiga%20toxin"

### BROKEN AND
http://rest.kegg.jp/find/genes/"shiga+toxin"
If query is in put in quotes.
 this result is the same as OR.

### BROKEN OR
http://rest.kegg.jp/find/genes/shiga%20toxin
If query is not placed in quotations.
 this result is the same as AND.

## Enzyme tests

http://rest.kegg.jp/find/enzyme/deoxy+1.1.1 AND works
http://rest.kegg.jp/find/enzyme/"deoxy%201.1.1" OR does not work
http://rest.kegg.jp/find/enzyme/"deoxy 1.1.1" OR does not work

0
http://rest.kegg.jp/find/enzyme/ec:2.4.99.1+ec:2.4.99.6 AND works but return nothing as no overlap
http://rest.kegg.jp/find/enzyme/"ec:2.4.99.1 ec:2.4.99.6" OR doesn't work
http://rest.kegg.jp/find/enzyme/ec:2.4.99.  works

