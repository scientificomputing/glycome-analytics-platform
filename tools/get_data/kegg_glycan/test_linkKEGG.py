__license__ = "MIT"

import unittest
import linkKEGG as lk


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_no_db_specified(self):
        """should return default glycan path example"""
        m = lk.linked_entries_from_kegg()
        self.assertIn("path", m)
        self.assertIn("G00001", m)


    def test_bad_db(self):
        """"""
        import urllib2

        with self.assertRaises(urllib2.HTTPError):
            m = lk.linked_entries_from_kegg("john", "sally")

    def test_bad_dbentry(self):
        """"""
        import urllib2

        with self.assertRaises(urllib2.HTTPError):
            m = lk.linked_entries_from_kegg("ko", "map000")

    def test_no_links_btn_dbs(self):
        """ should return an empty string, cannot write None to file"""
        m = lk.linked_entries_from_kegg("br", "gl:G10496")
        self.assertEquals(m,"")

    def test_enzyme_glycan_search_1(self):
         """
         test  "2.4.99.1 ec: " returns
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1 ec:")
         self.assertIn("2.4.99.1", m)
 
 
    def test_enzyme_glycan_search_2(self):
         """
         test  "2.4.99.1 ec: 2.4.99.6" returns
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1 ec: 2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
    def test_enzyme_glycan_search_3(self):
         """
         test  "2.4.99.1 ec:2.4.99.6" returns
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1 ec:2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
    def test_enzyme_glycan_search_4(self):
         """
         test  "2.4.99.1+2.4.99.6" returns. This time '+' is or.
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1+2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
    def test_enzyme_glycan_search_4b(self):
         """
         test  "2.4.99.1+ 2.4.99.6" returns. This time '+' is or.
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1+ 2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
    def test_enzyme_glycan_search_5(self):
         """
         test  "2.4.99.1 2.4.99.6" returns. Space also means or for the link db. 
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1 2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
 
    def test_enzyme_glycan_search_6(self):
         """
         test  "2.4.99.1+nana+2.4.99.6" returns. strangely when inserting junk it still works.
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1+nana+2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
    def test_enzyme_glycan_search_6b(self):
         """
         test  "2.4.99.1 nana 2.4.99.6" returns. strangely when inserting junk it still works.
         """
         m = lk.linked_entries_from_kegg("glycan", "2.4.99.1+nana+2.4.99.6" )
         self.assertIn("2.4.99.1", m)
         self.assertIn("2.4.99.6", m)
 
    def test_enzyme_glycan_search_7(self):
         """
         test  "\"2.4.99.1\""  is a bad request as it includes quotes. 
         """
         from urllib2 import HTTPError
         with self.assertRaises(HTTPError):
             m = lk.linked_entries_from_kegg("glycan", "\"2.4.99.1\"" )
 
