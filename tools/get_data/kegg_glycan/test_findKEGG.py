__license__ = "MIT"

import unittest
import findKEGG as fk


class SimpleUnitTest(unittest.TestCase):
    def setUp(self):
        import os

        os.environ["http_proxy"] = ""  # work around for IOError: [Errno url error] invalid proxy for http:
        pass

    def tearDown(self):
        pass

    def test_no_db_specified(self):
        """should raise error"""
        with self.assertRaises(IOError):
            m = fk.find_entries_in_KEGG([], " ")
        with self.assertRaises(IOError):
            m = fk.find_entries_in_KEGG("", " ")
        with self.assertRaises(IOError):
            m = fk.find_entries_in_KEGG(None, " ")

    def test_no_query_specified(self):
        """should raise error"""
        with self.assertRaises(IOError):
            m = fk.find_entries_in_KEGG("glycan", "")
        with self.assertRaises(IOError):
            m = fk.find_entries_in_KEGG("glycan", [])
        with self.assertRaises(IOError):
            m = fk.find_entries_in_KEGG("glycan", None)


    def test_bad_db(self):
        """should raise error"""
        import urllib2

        with self.assertRaises(urllib2.HTTPError):
            m = fk.find_entries_in_KEGG("john", "glucose")

    def test_unfindable_entry(self):
        """should return an empty string"""
        m = fk.find_entries_in_KEGG("glycan", "sally")
        emptystring=""
        self.assertItemsEqual(m,emptystring,"Expected Empty String for non-existent entry")

    def test_find_example(self):
        m = fk.find_entries_in_KEGG("glycan", "glucose")
        self.assertIsNotNone(m)
        self.assertIn("GDP-glucose", m)

    def test_query_has_newlines(self):
        """
        Assume a new line query is an AND query. 
        """
        m = fk.find_entries_in_KEGG("glycan", "glucose\nUDP")
        self.assertIsNotNone(m)
        print m
        self.assertIn("UDP-D-glucose", m)

    def test_query_has_newlines_and_a_space(self):
        """
        glucose\\n UDP. OR's should pass 
        """
        m = fk.find_entries_in_KEGG("glycan", "glucose\n UDP")
        self.assertIsNotNone(m)
        self.assertIn("UDP-D-glucose", m)

    def test_gene_specific_or(self):
        """
        test "shiga toxin" returns. 
         /find/genes/"shiga toxin" for keywords "shiga toxin"
        """
        m = fk.find_entries_in_KEGG("genes","shiga toxin")
        self.assertIsNotNone(m)
        #. not matching to all entries - that is silly, the db get updated!
        self.assertIn("shiga", m)
        self.assertIn("toxin", m)
        self.assertNotIn("stm:STM0284",m) # should not be found in the OR query

    def test_gene_specific_and(self):
        """
        test "shiga+toxin" returns. it should
         /find/genes/shiga+toxin  	for keywords "shiga" and "toxin"
        """
        m = fk.find_entries_in_KEGG("genes","shiga+toxin")
        self.assertIsNotNone(m)
        #. not matching to all entries - that is silly, the db get updated!
        self.assertIn("shiga", m)
        self.assertIn("toxin", m)
        self.assertIn("stm:STM0284",m) # should be found in the AND query

    def test_enzyme_specific_1(self):
        """
        test 2.4.99.1 returns
        """
        m = fk.find_entries_in_KEGG("enzyme","2.4.99.1")
        self.assertIsNotNone(m)
        #. not matching to all entries - that is silly, the db get updated!
        self.assertIn("2.4.99.1", m)

    def test_enzyme_specific_2(self):
        """
        test 2.4.99.6 returns
        """
        m = fk.find_entries_in_KEGG("enzyme","2.4.99.6")
        self.assertIsNotNone(m)
        #. not matching to all entries - that is silly, the db get updated!
        self.assertIn("2.4.99.6", m)

    def test_enzyme_specific_3(self):
        """
        test ec: 2.4.99.6 with space (tests stripping space functionality)
        """
        m = fk.find_entries_in_KEGG("enzyme","ec: 2.4.99.6")
        self.assertIsNotNone(m)
        #. not matching to all entries - that is silly, the db get updated!
        self.assertIn("2.4.99.6", m)

    def test_enzyme_specific_4(self):
        """
        test ec: 2.4.99. , should return 2.4.99.*
        """
        m = fk.find_entries_in_KEGG("enzyme","ec: 2.4.99.")
        self.assertIsNotNone(m)
        #. not matching to all entries - that is silly, the db get updated!
        self.assertIn("2.4.99.1", m)
        self.assertIn("2.4.99.6", m)
        self.assertIn("2.4.99.11", m)

    def test_enzyme_specific_5(self):
        """
        test ec: 2.4.99.1+2.4.99.6 . AND function, so returns nothing here. Do not expect an enzyme to be doubly classified. 
        """
        m = fk.find_entries_in_KEGG("enzyme","2.4.99.1+2.4.99.6")
        self.assertIsNotNone(m)
        emptystring=""
        self.assertItemsEqual(m,emptystring,"Expected Empty String for non-existent entry")

    def test_enzyme_specific_6(self):
        """
        test ec: 2.4.99.1 2.4.99.6 . OR function, should return something but does not (KEGG FAILURE?).
         note that trying this without "" will return the AND function result which is incorrect.
        """
        m = fk.find_entries_in_KEGG("enzyme","2.4.99.1 2.4.99.6")
        self.assertIsNotNone(m)
        #self.assertIn("2.4.99.1", m)
        emptystring=""
        print m
        self.assertItemsEqual(m,emptystring,"Expected Empty String for non-existent entry")


    def test_enzyme_specific_7(self):
        """
        test deoxy+1.1.1. . AND function, that should return something
        """
        m = fk.find_entries_in_KEGG("enzyme","deoxy+1.1.1")
        self.assertIsNotNone(m)
        print m
        self.assertIn("deoxy", m)
        self.assertIn("1.1.1", m)

    def test_enzyme_specific_8(self):
        """
        test deoxy 1.1.1. . OR function. should return deoxy or 1.1.1. but does not (KEGG FAILURE?). i
         note that trying this without "" will return the AND function result which is incorrect.
        """
        m = fk.find_entries_in_KEGG("enzyme","deoxy 1.1.1")
        self.assertIsNotNone(m)
        emptystring=""
        print m
        self.assertItemsEqual(m,emptystring,"Expected Empty String for non-existent entry")

