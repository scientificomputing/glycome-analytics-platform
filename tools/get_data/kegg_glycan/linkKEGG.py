__author__ = 'cbarnett'
__license__ = "MIT"
__version = "0.3.1"
# http://www.kegg.jp/kegg/rest/keggapi.html


def linked_entries_from_kegg(targetdb="glycan", sourcedb="pathway"):
    """
    :param targetdb:
    :param sourcedb:
    :return: string  of linked entries
    """
    import urllib2

    uri = 'http://rest.kegg.jp/link/'

    import re
    p = re.compile(' *\+ *') # ensure no unneccessary space in an AND query
    sdbfix = p.subn('+', sourcedb)
    sourcedb=sdbfix[0]

    if ' ' in sourcedb:
        sourcedb = sourcedb.replace(' ', '%20')  # previous behaviour was ignoring text after a space, rather convert to '%20' and pass on to KEGG REST service

    fulluri = uri + targetdb + "/" + sourcedb
    try:
        response = urllib2.urlopen(fulluri).read()
    except Exception as e:
        raise urllib2.HTTPError(e.url, e.code, e.msg, e.hdrs, e.fp)
    if str(response.strip()) == "":
        return ""
    return response


if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-t", action="store", type="string", dest="t", default="glycan",
                      help="target db name pathway | brite | module | ko | genome | <org> | compound | glycan | reaction | rpair | rclass | enzyme | disease | drug | dgroup | environ")
    parser.add_option("-s", action="store", type="string", dest="s", default="pathway",
                      help="source db name or db entry e.g. map00010")
    parser.add_option("-o", action="store", type="string", dest="o", default="linked_entries.txt",
                      help="linked entries output in text format")
    (options, args) = parser.parse_args()
    try:
        outstream = file(options.o, 'w')
    except Exception as e:
        raise IOError(e, "the output file cannot be opened. Use -h flag for help")
    linked = linked_entries_from_kegg(targetdb=options.t, sourcedb=options.s)
    try:
        outstream.write(linked)
    except Exception as e:
        raise IOError(e, "cannot open output files. -h flag for help")
    finally:
        outstream.close()


