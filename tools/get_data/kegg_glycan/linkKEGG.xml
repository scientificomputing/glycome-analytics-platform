<tool id="glytools_kegg_link" name="GAP Cross-reference with KEGG databases " version="0.3.2">
    <description> searches KEGG for related database entries</description>
    <requirements>
        <requirement type="package" version="1.0">python_for_glycan_tools</requirement>
    </requirements> 
    <command interpreter="python"><![CDATA[linkKEGG.py -t $targetdb -s $source -o $output ]]></command>
    <inputs>
        <param name="targetdb" type="select" label="Target Database">
          <option value="glycan" selected="true"> target KEGG glycan database</option>
          <option value="pathway" selected="false"> target KEGG pathway database</option>
          <option value="brite" selected="false"> target KEGG brite database</option>
          <option value="module" selected="false"> target KEGG module database</option>
          <option value="ko" selected="false"> target KEGG ko database</option>
          <option value="genome" selected="false"> target KEGG genome database</option>
          <option value="compound" selected="false"> target KEGG compound database</option>
          <option value="reaction" selected="false"> target KEGG reaction database</option>
          <option value="rpair" selected="false"> target KEGG rpair database</option>
          <option value="rclass" selected="false"> target KEGG rclass database</option>
          <option value="enzyme" selected="false"> target KEGG enzyme database</option>
          <option value="disease" selected="false"> target KEGG disease database</option>
          <option value="drug" selected="false"> target KEGG drug database</option>
          <option value="dgroup" selected="false"> target KEGG dgroup database</option>
          <option value="environ" selected="false"> target KEGG environ database</option>
        </param>
        <param name="source" type="text" value="ec:2.4.1.11" optional="false" label="Source database or entry" help="Source database or db entry to use to search target db. Example dbsource is genome. Example db entry is map00010"/>
    </inputs>
    <outputs>
        <data format="txt" name="output" label="linked entries from KEGG database search"/>
    </outputs>
 <citations>
     <!--  -->
     <citation type="doi">10.1002/0471250953.bi0112s38</citation>
  </citations>
 <tests>
    <test>
      <param name="targetdb" value="glycan" />
      <param name="source" value="ec:2.4.1.11" />
      <output name="output" file="link_test1.txt" />     
  </test>
  </tests>
    <help> <![CDATA[ 
.. class:: infomark

**What this tool does**

This tool find connections between entries or db's found at KEGG. For instance which enzymes work on UDP-Glucose, or which reactions is UDP-Glucose involved in,
or which glycans are acted by an enzyme that acts on UDP-Glucose (ec:2.4.1.11).

----

.. class:: infomark

**Example 1**

target and source sequence::

 enzyme
 gl:G10608 

output sequence::

 gl:G10608    ec:2.4.1.11
 gl:G10608    ec:2.4.1.12
 gl:G10608    ec:2.4.1.13
 gl:G10608    ec:2.4.1.14
 gl:G10608    ec:2.4.1.15
 gl:G10608    ec:2.4.1.183
 gl:G10608    ec:2.4.1.188
 gl:G10608    ec:2.4.1.34
 gl:G10608    ec:2.4.1.53
 gl:G10608    ec:2.4.1.80
 
.. class:: infomark

**Example 2**

target and source sequence::

 reaction
 gl:G10608 

output sequence::

 gl:G10608    rn:R06020
 gl:G10608    rn:R06023
 gl:G10608    rn:R06036
 gl:G10608    rn:R06043
 gl:G10608    rn:R06045
 gl:G10608    rn:R06051
 gl:G10608    rn:R06062
 gl:G10608    rn:R06073
 gl:G10608    rn:R06181
 gl:G10608    rn:R06226
 gl:G10608    rn:R06275
 gl:G10608    rn:R09315
 gl:G10608    rn:R09316

.. class:: infomark

**Example 3**

target and source sequence::

 glycan
 ec:2.4.1.11

output sequence::

 ec:2.4.1.11    gl:G10495
 ec:2.4.1.11    gl:G10608
 ec:2.4.1.11    gl:G10619

]]>

</help>

</tool>
