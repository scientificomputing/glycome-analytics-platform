[TOC]

# 1. cfg_get_glycan_from_cells.py

Retrieve glycan profiling data from the CFG.

With focus on retrieving the annotated mass spec data (.msa files)

## Works with Galaxy?
Yes. see [getcfgcells.xml](getcfgcells.xml)

## Command line usage
```
../../virtualpy/bin/activate
python cfg_get_glycan_from_cells.py
```

This will use the default settings and by default pull all data from the CFG. This may take some time.
The CFG may be unimpressed, if you do this too often.

To specify which data you would like to retrieve :
```
../../virtualpy/bin/activate
cfg_get_glycan_from_cells.py -s "$species" -p "$investigator" -g "$glycantype" -c "$celltype" -o "$html_file" --filepath "msa_output"
```

## Help
```
../../virtualpy/bin/activate
python cfg_get_glycan_from_cells.py -h
```

## Unit Testing?
Not yet! Although some testing ha been done using the htmlfortesting.html and short-htmlfortesting.html. This code has also been used within Galaxy to retrieve data.
So it _should work just fine..._

In principle:

```
../../virtualpy/bin/activate
python  test_cfg_get_glycan_from_cells.py
```
