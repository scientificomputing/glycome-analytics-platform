
#GetCFG


Current implementation uses data discovery

* Galaxy note - each msa file is exposed as a new dataset in your history, thus links in this html file do not work in Galaxy. You may need to refresh your history to see all datasets.


## Dev notes and thoughts

## Dealing with multiple output data sets

https://wiki.galaxyproject.org/Admin/Tools/Multiple%20Output%20Files

2 options, either html with links to msa files or 'data discovery'

Seems like HTML is best
### HTML
Unsure if can be used properly in workflows?
The application must create and write valid html to setup the page $html_file seen by the user when they view (eye icon) the file. It must create and write that new file at the path passed by Galaxy as the $html_file command line parameter. All application outputs that will be included as links in that html code should be placed in the specific output directory $html_file.files_path passed on the command line. The external application is responsible for creating that directory before writing images and files into it. When generating the html, The files written by the application to $html_file.files_path are referenced in links directly by their name, without any other path decoration 

#### 
html works but html file is considered the history item, the msa files are not accessible in the workflow.
Idea:
Once the html has been generated the number of msa will be known. 
create another xml to output the number of msa files found (or possibly generate this in a txt file here?)
then create another xml to 'output' msa data as individual history items. 
will not work as the filter is usually a boolean condition rather than an integer condition. Also a small number of outputs is easily supported but not much more. 

Not sure how this will fit into a workflow. Maybe this will be useful for creating data libraries -> no as msa files are not accessible. 

### DATA DISCOVERY
Cons :  discovered datasets cannot be used with workflows and require the user to refresh their history before they are shown.
Sure but at least you get access to the msa files. These can then be exported to other histories quite easily and/or used to make data libraries. 

Data discovery works great. 

