__author__ = "Chris Barnett"
__version__ = "0.3"
__license__ = "SCRU"

from BeautifulSoup import BeautifulSoup
import mechanize


class PrettifyHandler(mechanize.BaseHandler):
    def http_response(self, request, response):
        if not hasattr(response, "seek"):
            response = mechanize.response_seek_wrapper(response)
        # only use BeautifulSoup if response is html
        if response.info().dict.has_key('content-type') and ('html' in response.info().dict['content-type']):
            soup = BeautifulSoup(response.get_data())
            response.set_data(soup.prettify())
        return response


def mechanise_cfg_cells(species, investigator, glycantype, celltype, page='http://www.functionalglycomics.org/glycomics/common/jsp/samples/searchSample.jsp?templateKey=2&12=CellType&operation=refine', debug=False):
    """
    Use mechanise to submit input glycan and formats to the new converter tool at RINGS
    """
    import mechanize
    import cookielib

    # create a Browser
    br = mechanize.Browser()
    br.add_handler(PrettifyHandler())

    #  handle cookies - Cookie Jar
    cj = cookielib.LWPCookieJar()
    br.set_cookiejar(cj)

    # Browser options
    br.set_handle_equiv(True)
    br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)

    # Follows refresh 0 but not hangs on refresh > 0
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)

    # Want debugging messages?
    br.set_debug_http(debug)
    br.set_debug_redirects(debug)
    br.set_debug_responses(debug)

    br.addheaders = [('User-agent',
                      'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]

    # Open site
    #page = 'http://www.functionalglycomics.org/glycomics/common/jsp/samples/searchSample.jsp?templateKey=2&12=CellType&operation=refine'
    br.open(page)

    # Show the response headers
    response_info = br.response().info()

    # select the input form
    br.select_form(nr=0)
#    for form in br.forms():
#        print "Form name:", form.name
#        print form
    # read user glycan and submit
    # glycandata = inputstream.read() # read into a variable as I need it later
    #    br.form["5"] = "" # species
    #    br.form["15"] = "" # participating investigator
    #    br.form["10"] = "" # glycan type
    #    br.form["9"] = "" # cell type

    #. select appropriate input on form
    if species == "all":
        pass
    else:
        control = br.form.find_control("5")
        br[control.name] = [species] # could use br["5"] but I think this is way is more descriptive

    if investigator == "all":
        pass
    else:
        control = br.form.find_control("15")
        br[control.name] = [investigator]

    if glycantype == "all":
        pass
    else:
        control = br.form.find_control("10")
        br[control.name] = [glycantype]

    if celltype == "all":
        pass
    else:
        control = br.form.find_control("9")
        br[control.name] = [celltype]


    br.submit()

    response = br.response().read()  # the converted file!

    if response is None or str(response.strip()) == '':
        raise IOError("empty response")
    return response


def clean_json_response(response):
    """
        # look at json  status has failure status, submitData has original data and result has output with format

    :param response: json from RINGS convert
    :return: a list of glycan structures
    """
    import json
    import re
    import StringIO

    # RINGS bug, additional data returned with JSON format. Clean this up
    jsonoutputasfilehandle = StringIO.StringIO(''.join(response))
    keeplines = []  # only keep lines that look like JSON
    for line in jsonoutputasfilehandle.readlines():
        if line[0] == "[":
            keeplines.append(line)

    response2 = ''.join(keeplines)
    # RINGs bug. Now remove junk data appended to the JSON lines for example "}}]GLC "
    p = re.compile('(}}].*)')
    jsontobeparsed = p.subn('}}]', response2)

    # load json
    loaded_as_json = json.loads(jsontobeparsed[0])
    structures = []

    # there could be multiple structures so iterate over, structures are numbered using the No tag.

    # .. matches for  linearcode and eol fixes
    linearcodefix = re.compile('(;$)')
    eolfix = re.compile('(\n$)')

    for glycan in loaded_as_json:
        if str(glycan["status"]) == "false":
            raise IOError(glycan["result"][
                              "message"])  # raise error even though not all structures need be broken. It is no use letting through a broken structure. important to let the user know.
        else:
            # bugfix remove ";" from end of LinearCode
            lcfixed = linearcodefix.subn('\n', str(glycan["result"]["structure"]))[0]
            # now remove all \n at end of each sequence. Some have, some don't, so remove all and add later
            eolfixed = eolfix.subn('', lcfixed)[0]
            structures.append(eolfixed)

    return structures


def get_data_from_soup(html, sitename="http://www.functionalglycomics.org"):
    """
     get msa from soup

    :param html: html from posting to mcaw
    :return: msa
    """
    from BeautifulSoup import BeautifulSoup
    import urllib2
    import re

    if html is None or html == [] or html == "":
        raise IOError("input stream is empty")
    soup = BeautifulSoup(html)
    pkcf = ""  # set to empty
    # tr class="gtTablePresentation"
    all_data = []
    for row in soup.findAll('tr', {"class": "gtTablePresentation"}):
        cells = row.findAll("td")
        if len(cells) == 16:  # double check we know what this pages looks like :)
            # print cells
            all_data.append(create_data_entry(cells, sitename))
        else:
            #print "NOT DATA"  # change this to log
            pass

    return all_data


##        #print "THE ONE ", link, "\n"
##        try:
#            #if "gtTablePresentation" in link.get('class'):
##
##                #print "THE ONE ", link, "\n"
###                for descendant in link.descendants:
###
###                    print "EACH ", descendant
#
##                print "CONTENTS: ", link.contents
##                print "CHILDREN: ", link.children
##
###                print link.
##                print "DESCENDANTS: ", link.descendants
##                print "SIBLING: ", link.next_sibling
##                print len(list(link.children))
##                print len(list(link.descendants))
##                print len(list(link.contents))
##
##        except:
##            pass
#
##    for link in soup.findAll("td"):
##        try:
##            if "glycoEnzymeTablePresentation" in link.get('class'):
##                print link.next_sibling.next_sibling.next_sibling
##                #print link.children
##
##        except:
##            pass
#        #if "gtTablePresentation" in link.get('class'):
#        #    print link
##        if "pkcf" in link.get('href'):
##            pkcflink = link.get('href')
##            pkcf = urllib2.urlopen(pkcflink).read()
#
##    for link in soup.findAll(href=re.compile(".*msa")):
##        try:
##            if "msa" in link.get('href'):
##                print link
##        except:
##            pass
#        #print link
#    return


def create_data_entry(cells, sitename):
    """

    :param cells: list of beautiful soup data parsed from the CFG
    :return: dictionary with cleaned cfg data
    """
    from BeautifulSoup import BeautifulSoup
    try:
        species = cells[0].find(text=True).strip()
        comments = cells[1].find(text=True).strip()
        celltype = cells[2].find(text=True).strip()
        glycantype = cells[3].find(text=True).strip()
        investigator = cells[4].find(text=True).strip()
        metadata_experimental_details = cells[5]
        image_all = cells[6]
        image_jpg = helper_clean_links(cells[7], sitename)
        image_pdf = helper_clean_links(cells[8], sitename)

        rawdata_all = cells[9]
        rawdata_msd = helper_clean_links(cells[10], sitename)
        rawdata_msa = helper_clean_links(cells[11], sitename)
        rawdata_dat = helper_clean_links(cells[12], sitename)
        rawdata_msms = helper_clean_links(cells[13], sitename)
        rawdata_interactive = helper_clean_links(cells[14], sitename)
        resource_request = helper_clean_links(cells[15], sitename)

        jsonobject = {
            'species': species,
            'celltype': celltype,
            'glycantype': glycantype,
            'investigator': investigator,
            'image_jpg': image_jpg,
            'image_pdf': image_pdf,
            'rawdata_msd': rawdata_msd,
            'rawdata_msa': rawdata_msa,
            'resource_request': resource_request
        }
        return jsonobject
    except Exception as e:
        print e


def helper_clean_links(tag, sitename):
    """

    :param list_of_links: list of links to images or mass spec. may be of size 0 - n
    :param sitename: website name to prepend to relative links
    :return:
    """
    from BeautifulSoup import BeautifulSoup
    new_list_of_links = []
    if tag is [] or None:
        return []
    else:
        list_of_links = tag.findAll('a')
        if list_of_links is [] or None:
            return []

        for link in list_of_links:
            new_list_of_links.append(str(sitename) + str(link.get("href")))

        return new_list_of_links


def helper_clean_rawdata_links(list_of_links, sitename):
    """

    :param list_of_links: list of links to rawdata. may be of size 0 - 1
    :param sitename: website name to prepend to relative links
    :return:
    """
    from BeautifulSoup import BeautifulSoup

    new_list_of_links = []
    if list_of_links is [] or None:
        return []
    else:
        for link in list_of_links:
            new_list_of_links.append(str(sitename) + str(link.get("href")))

        return new_list_of_links


def helper_write_to_json(data, output, the_indent=4):
    import json
    j2 = json.dumps(data, indent=the_indent)
    f2 = open(output, 'w')
    print >> f2, j2
    f2.close()

def helper_write_to_html(data, output, output_path, options):
    import os
    from jinja2 import Environment, FileSystemLoader
    #env = Environment(loader=PackageLoader(os.path.dirname(os.path.abspath(__file__)), 'templates'))
    currentpath = os.path.dirname(os.path.abspath(__file__))
    env = Environment(loader=FileSystemLoader(os.path.join(currentpath, 'templates')))
    #env = Environment(FileSystemLoader(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'templates')))
    template = env.get_template('base.html')
    
    for entry in data:  # this copied from download_and_save_msa
        if len(entry["rawdata_msa"]) == 0:
            entry['href']=""
            entry['caption']="no msa data found for "+"_".join([entry["species"],entry["celltype"],entry["investigator"].split(",")[0], entry["glycantype"]] ) 
        else:
            try:
                entry['href'] = "_".join([entry["species"],entry["celltype"],entry["investigator"].split(",")[0], entry["glycantype"],os.path.basename(entry["rawdata_msa"][0] )] )
                entry['caption'] =  "_".join([entry["species"],entry["celltype"],entry["investigator"].split(",")[0], entry["glycantype"]])

            except Exception as e:
                raise e
    with open(output,'w') as f:
        f.write(template.render(title='Mass Spec Annotated Files from the CFG', items=data,species=options["species"], investigator=options["pi"], glycan=options["glycan"], cell=options["cell"] ))


def download_and_save_msa(parseddata, filepath):
    """

    :param parseddata: dictionary containing links to msa data
    :return:
    """
    import os
    import urllib2

    if not os.path.exists(filepath):
        os.makedirs(filepath)
    for entry in parseddata:
        if len(entry["rawdata_msa"]) == 0:
            print "no msa for ", entry["species"], entry["celltype"], entry["investigator"], entry["glycantype"]
        else:
            try:
                filename = "_".join([entry["species"],entry["celltype"],entry["investigator"].split(",")[0], entry["glycantype"],os.path.basename(entry["rawdata_msa"][0] )] )
            except Exception as e:
                raise e

            with open(os.path.join(filepath,filename),'w') as f:
                f.write(urllib2.urlopen(entry["rawdata_msa"][0]).read())
        #print entry["rawdata_msa"]
        #print os.path.basename(entry["rawdata_msa"][0])
        #print filename

if __name__ == "__main__":
    from optparse import OptionParser

    usage = "usage: python %prog [options]\n"
    parser = OptionParser(usage=usage)
    parser.add_option("-o", action="store", type="string", dest="o", default="output.html",
                      help="html output glycan summary file (output)")
    parser.add_option("--filepath", action="store", type="string", dest="filepath", default="msa_output",
                      help="galaxy file path to be prepended")

    parser.add_option("-j", action="store", type="string", dest="j", default="output.json",
                      help="output glycan summary file (output)")
    parser.add_option("-s", action="store", type="string", dest="s", default="all",
                      help="species (all)")
    parser.add_option("-p", action="store", type="string", dest="p", default="all",
                      help="participating investigator (all)")
    parser.add_option("-g", action="store", type="string", dest="g", default="all",
                      help="glycan type (all)")
    parser.add_option("-c", action="store", type="string", dest="c", default="all",
                      help="cell type (all)")
    parser.add_option("-t", action="store_true", dest="t", default=False,
                      help="cell type (all)")
    (options, args) = parser.parse_args()

    if options.t:
        #. for testing purposes read from example webpage rather than querying CFG
        #. only useful for initial testing. afterwards it will not filter and then will download all msa data (NOT IDEAL)
	testingfile="htmlfortesting.html"
        try:
            instream = file(testingfile, 'r')
        except Exception as e:
           raise IOError(e, "the input file specified does not exist. Use -h flag for help")
        #. read example webpage using selections
	m = instream.read() 
    else:
        #. read webpage using selections
        m = mechanise_cfg_cells(options.s, options.p, options.g, options.c)



    #. parse response for all useful data like links to msa
    s = get_data_from_soup(m)
    #. write out json of parsed data dictionary
    helper_write_to_json(s, options.j)
    #. write out html to specified dir
    options_dict={"species":options.s,"pi":options.p,"glycan":options.g,"cell":options.c}
    helper_write_to_html(s, options.o, options.filepath,options_dict)
    #. download the msa files and save
    download_and_save_msa(s,options.filepath)
