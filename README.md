[TOC]

# Glycome Analytics Platform

Tools and resource for accessing glycan data, detecting formats and analysing glycomes.
These tools have been designed for use with Galaxy Project.

PUBLIC REPO - https://bitbucket.org/scientificomputing/glycome-analytics-platform

PRIVATE REPO - https://bitbucket.org/rxncor/glycantools


# Getting Started

## Quickstart 

[Use the public testing instance and find out more about the glycan related to ST6Gal1](docs/Tutorials/Quickstart/Quickstart.md)

A public testing instance is available in the [cloud](http://ec2-52-26-82-234.us-west-2.compute.amazonaws.com:8080).
This instance is for testing, please do not use for confidential or critical research. The sqlite database is being used for this testing instance, multiple concurrent jobs cannot be processed and this may lead to job failure. 

## Galaxy

### Get / Use Galaxy
See more at [Galaxy Project](https://galaxyproject.org/)

A brief overview of a installing a minimal cloud instance on AWS is available [here](docs/Galaxy_cloud_install.md)

### Install Glycan tools from the Galaxy Toolshed

#### Use Galaxy Test ToolShed

GAP is currently available on the [Galaxy Test ToolShed](https://testtoolshed.g2.bx.psu.edu/) under Web Services, or search for gap.

To use the Test Toolshed your local Galaxy Admin must have added

```
<tool_shed name="Galaxy Test Tool Shed" url="https://testtoolshed.g2.bx.psu.edu/"/>
```

to config/tool_sheds_conf.xml


Install as follows:

- First install gap_test_datatypes
- Then either install all tools as one bundle (gap_test_all)
- or install each set of tools to a separate toolpanel section.
    - gap_test_get
    - gap_test_convert      
    - gap_test_manipulate
    - gap_test_join
    - gap_test_extract_features


#### Use Galaxy Main ToolShed

Tools have been [published](docs/Galaxy_Toolshed_publish.md) in the Galaxy Toolshed.
To install tools into Galaxy from a Toolshed, [see here](https://wiki.galaxyproject.org/Admin/Tools/AddToolFromToolShedTutorial)

GAP is currently available on the [Galaxy ToolShed](https://toolshed.g2.bx.psu.edu/) under Web Services, or search for gap.

Install as follows:

- First install gap_datatypes
- Then either install all tools as one bundle gap_all_glycan_tools



### Install Glycan tools to a Galaxy Toolshed

For information on Installing Galaxy using a local Toolshed, [see here](docs/Galaxy_manual_install.md).
This is useful for testing and development.


## Command line usage
Many of the available tools can be used from the command line. This is most useful for testing the tools outside of Galaxy.


### Requirements
* Python 2.7 and development tools
* requirements as per requirements.txt (in Galaxy these are automatically installed via the Toolshed)
* pyqt4 (manual install, for rendering with ete2)

It is recommended that you install a virtualenv or use conda.
#### Virtual Env in Red Hat Enterprise Linux or similar
##### Install virtual environment for Python
`sudo yum install python python-devel python-virtualenv`

##### Create a virtual environment
`virtualenv virtualpy`
##### Install necessary packages
`virtualpy/bin/pip install -r requirements.txt`

# Available Tools

## Supported Glycan Formats

The following formats are available when GAP is installed into Galaxy:

- KCF
- GlycoCT
- GlycoCT XML
- GlydeII XML
- LINUCS
- IUPAC
- LinearCode
- MSA
- WURCS

For more info see [here](datatypes/README.md)


## Retrieval of glycan data
For example access to KEGG and CFG data sources, [see here](tools/get_data/Overview_get_data.md)

## Convert and Detect Glycan Formats
Convert between formats such as GLYDE, LINUCS and so forth, [see here](tools/convert_detect_formats/Overview_convert_detect_formats.md).

## Manipulation of data
Manipulation of glycan data e.g. rename KCF. [see here](tools/manipulate/Overview_manipulate.md).

## Join, subtract grouping of data
Set-like manipulations of glycan data. [see here](tools/join_subtract_group/Overview_join_subtract_group.md).

## Display feature of glycomes
MCAW, miner and similar analysis of glycan data. [see here](tools/extract_display_features/Overview_extract_display_features.md).


# Authors
 - Chris Barnett @rxncor007

# License
[The MIT License](LICENSE.md)

# Testing
Functional tests are available for each tool. Look in each tool directory for 'test\*.py '.
Tests have been conducted in RHEL 7.2 using python 2.7

# Contributing
To contribute, please fork this project and submit your changes via a pull request. 

The current structure of the project is as follows:
```
./datatypes                           <---- glycan datatypes
./docs                                <---- documentation
./example_data                        <---- example data sets
./helper_scripts                      <---- scripts
tools                                 <---- glycan tools
├── convert_detect_formats            <---- subcategory
│
├── extract_display_features
│
├── get_data
│   ├── kegg_glycan                  <---- specific set of tools
│   │   ├── findKEGG.py              <---- python script 
│   │   ├── findKEGG.xml             <---- Galaxy wrapper
│   │   ├── README_KEGG.md           <---- docs for tool
│   │   ├── test_findKEGG.py         <---- test for tool
│   └── Overview_get_data.md         <---- docs for this category of tools
│
├── join_subtract_group
│
├── manipulate
│
└── tool_dependencies.xml            <---- python tool dependencies
./bundle.sh                          <---- bundles tools and deps for Galaxy
./validate_xml.sh                    <---- lint xml using Galaxy XSD
```


# Troubleshooting and Issues
[Troubleshooting](docs/Troubleshooting.md)

Report issues using the [issue tracker](https://bitbucket.org/scientificomputing/glycome-analytics-platform/issues)

# Development
[Development](docs/Development.md)
