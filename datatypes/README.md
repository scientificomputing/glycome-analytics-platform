
Custom glycan data types for galaxy
===================================

New glycan data types for galaxy, included as part of the glycan tools repo instead of being included manually (as done previously)
Ideas from http://gregvonkuster.org/galaxy-tool-shed-including-custom-datatypes-repositories/

Supported data types include (copied from datatypes_conf.xml):

    <sniffer type="galaxy.datatypes.glycan:kcf"/>
    <sniffer type="galaxy.datatypes.glycan:glycoct"/>
    <sniffer type="galaxy.datatypes.glycan:glycoct_xml"/>
    <sniffer type="galaxy.datatypes.glycan:glydeii"/>
    <sniffer type="galaxy.datatypes.glycan:linucs"/>
    <sniffer type="galaxy.datatypes.glycan:iupac"/>
    <sniffer type="galaxy.datatypes.glycan:linearcode"/>
    <sniffer type="galaxy.datatypes.glycan:msa"/>
    <sniffer type="galaxy.datatypes.glycan:wurcs"/>

